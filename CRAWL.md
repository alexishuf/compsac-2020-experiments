Step-by-step crawling
=====================

This section details the crawl of each endpoint and how cralws depend on 
one another. Consider using the `crawl-all.sh` script unless debugging or 
curious.

From the parent dir, get all SIAFI organizations first:
```bash
./run.sh CRAWL --out-dir crawl --array-file crawl/siafi.json \
    'http://www.transparencia.gov.br/api-de-dados/orgaos-siafi{?pagina}'
```

Next, get all procurements from 2019 for every SIAFI organization:
```bash
./run.sh CRAWL --delay-ms 1000 --assume-last 10 --out-dir crawl/licitacoes \
    --start-date 2019-01-01 --end-date 2019-12-31 \
    --array-param codigoOrgao=crawl/siafi.json?codigo \
    'http://www.transparencia.gov.br/api-de-dados/licitacoes{?dataInicial,dataFinal,codigoOrgao,pagina}'
```

Get all contracts which came into effect in 2019. Many contracts span several
years. Therefore breaking the period into one-month queries will loose results
. The end date used should be a bogus one, otherwise no contract ending after 
2019 will be returned, even though it was active in 2019.
```bash
./run.sh CRAWL --delay-ms 1000 --assume-last 10 --out-dir crawl/contratos \
    --start-date 2019-01-01 --end-date 2099-12-31 --no-month-limit \
    --array-param codigoOrgao=crawl/siafi.json?codigo \
    'http://www.transparencia.gov.br/api-de-dados/contratos{?dataInicial,dataFinal,codigoOrgao,pagina}'
```

One of the scenario queries requires the `modalidadeCompra/descricao` field. 
This field is not included in the above API (but it should!). To get this 
field, we need to issue an additional query for each contract to get its 
description from its ID. The problem is, there are ~29437 contracts. In 
order to be polite, we re-download only the contracts signed by organization 
26246 (the university):  
```bash
mkdir -p crawl/contratos-id          # required for | tee 
./run.sh CRAWL --delay-ms 1000 --out-dir crawl/contratos-id  \
    --no-paging --naming-param id \
    --obj-param-dir crawl/contratos \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path-filter 'unidadeGestora/orgaoVinculado/codigoSIAFI=26246' \
    --obj-param-path 'id=id' \
    'http://www.transparencia.gov.br/api-de-dados/contratos/id{?id}' \
    2>&1 | tee crawl/contratos-id/run.log
```

There are also two undocumented APIs that need to be crawled. The first gets 
small objects that contain a description and an internal ID for every 
contract counterpart. The input for queries is the `fornecedor/codigoFormatado` 
path in contracts. Again, only contracts from organization 26246 are used.  
```bash
mkdir -p crawl/fornecedor-autocomplete          # required for | tee 
./run.sh CRAWL --delay-ms 1000 --out-dir crawl/fornecedor-autocomplete \
    --no-paging \
    --obj-param-dir crawl/contratos \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path-filter 'unidadeGestora/orgaoVinculado/codigoSIAFI=26246' \
    --obj-param-path 'q=fornecedor/codigoFormatado' \
    'http://www.portaldatransparencia.gov.br/criterios/contratos/fornecedor/autocomplete{?q}' \
    2>&1 | tee crawl/fornecedor-autocomplete/run.log
```


The second undocumented API returns the participation of a service/goods 
provider in procurements, when given the id obtained in the previous crawl:
```bash
mkdir -p crawl/participante-licitacao          # required for | tee 
./run.sh CRAWL --delay-ms 4000 --out-dir crawl/participante-licitacao \
    --first-page 0 --page-param offset --page-increment 15 \
    --root-path data --anon-name --inject-property idFornecedor=id \
    --obj-param-dir crawl/fornecedor-autocomplete \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path 'id=id' \
    'http://www.portaldatransparencia.gov.br/pessoa-juridica/{id}/participante-licitacao/resultado{?id,offset}&paginacaoSimples=true&tamanhoPagina=15&direcaoOrdenacao=desc&colunaOrdenacao=numeroLicitacao&colunasSelecionadas=linkDetalhamento%2Corgao%2CunidadeGestora%2CnumeroLicitacao%2CdataAbertura' \
    2>&1 | tee crawl/participante-licitacao/run.log
```