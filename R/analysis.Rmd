---
title: "COMPSAC 2020"
author: "Alexis"
date: "2020-02-17"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(ggplot2);
library(tidyr);
library(dplyr);
library(Rmisc);
library(boot);

options(scipen=999)

theme_set(theme_bw(base_family = "Times", base_size = 9));
theme_update(legend.key.size = unit(0.25, "cm"),
             legend.text = element_text(size=9),
             legend.margin = margin(),
             legend.box.margin = margin());

pdftee <- function(filename, p, hcm = 10.76/2, wcm = 10.76) {
  pdf(filename, height = hcm/2.54, width = wcm/2.54);
  print(p);
  dev.off();
  print(p);
}

createStateFrame <- function (df, minRun, varNames) {
  dfStat <- expand_grid(ExperimentName=unique(df$ExperimentName), 
                        Var=varNames);
  dfStat$avg <- NA
  dfStat$avg.lo <- NA
  dfStat$avg.hi <- NA
  for (i in 1:nrow(dfStat)) {
    varName <- dfStat$Var[i]
    f <- function(d, i) {
      return(mean(d[i]));
    }
    df2 <- df[df$ExperimentName==dfStat$ExperimentName[i] & 
                df$ExperimentRun >= minRun &
                df$Var==dfStat$Var[i],]
    if (nrow(df2) > 0) {
      set.seed(793600)
      obj <- boot(df2$Value, f, R=1000)
      ci <- boot.ci(obj, type=c('bca'))
      dfStat$avg[i] <- obj$t0
      dfStat$avg.lo[i] <- ci$bca[4];
      dfStat$avg.hi[i] <- ci$bca[5];
    }
  }
  dfStat$ExperimentName <- factor(dfStat$ExperimentName)
  dfStat$Var <- factor(dfStat$Var)
  return(dfStat);
}

addMsVar <- function (df, varName) {
  epilog <- data.frame(df[df$Var==varName,])
  epilog$Var <- gsub('Secs$', 'Ms', varName)
  epilog$Value <- epilog$Value*1000
  return(rbind(df, epilog))
}

```

## Load experiments data

Load TDB parse & load results.

```{r tdbload}
df1 <- read.csv('../experiments/tdb-setup.csv');
df1$ExperimentName <- 'HOT'
df2 <- read.csv('../experiments/tdb-setup-flushed.csv');
df2$ExperimentName <- 'COLD'
setup.df <- rbind(df1, df2)

setup.df <- addMsVar(addMsVar(setup.df, 'tdbloaderSecs'), 'jsonParseSecs')
setup.dfStat <- createStateFrame(setup.df, 0, c('tdbloaderSecs', 'jsonParseSecs'))
#setup.dfStat

plot.df <- data.frame(setup.dfStat)
plot.df$Var <- gsub('jsonParseSecs', 'JSON parse into RDF', plot.df$Var)
plot.df$Var <- gsub('tdbloaderSecs', 'TDB load', plot.df$Var)
plot.df$Var <- factor(plot.df$Var, levels=c('JSON parse into RDF', 'TDB load'))

ggplot(plot.df, aes(x=ExperimentName, fill=Var, y=avg)) +
  geom_bar(stat='identity', position=position_dodge()) +
  geom_errorbar(aes(ymin=avg.lo, ymax=avg.hi), position=position_dodge())
```

Get all measurement files into a single data frame. Fill in `totalParseMs` for TDB-experiments from the `tdb-setup-flushed.csv` file, fetching the value from the same `ExperimentRun`. In the event there are more `ExperimentRun` values in `meas.df` (or there are missing runs in `rdb-setup-flushed.csv`), fill using the average.

```{r measures.load}
meas.df <- rbind(
  read.csv('../experiments/proc4contr/measurements.csv'),
  read.csv('../experiments/proc_of_contractor/measurements.csv'),
  read.csv('../experiments/tdb_proc4contr/measurements.csv'),
  read.csv('../experiments/tdb_proc_of_contractor/measurements.csv')
);

expNames <- unique(meas.df$ExperimentName);
expNames <- expNames[grep('^TDB.*', expNames)]
epilog <- expand.grid(ExperimentName=expNames,  
                      ExperimentRun=unique(meas.df$ExperimentRun), 
                      Var=c('tdbloaderSecs', 'jsonParseSecs'), 
                      Value=as.double(NA))

# fetch tdbloader* and jsonParse* values from setup.df
for (i in 1:nrow(epilog)) {
  run <- epilog$ExperimentRun[i]
  varName <- as.character(epilog$Var[i])
  secs <- setup.df[setup.df$ExperimentName=='COLD' & 
                    setup.df$ExperimentRun==run & 
                    setup.df$Var==varName,]$Value[1]
  if (is.na(secs)) {
    secs <- setup.dfStat$avg[setup.dfStat$ExperimentName=='COLD' & setup.dfStat$Var == varName][1]
  }
  epilog$Value[i] <- secs;
}
epilog <- addMsVar(addMsVar(epilog, 'tdbloaderSecs'), 'jsonParseSecs')
meas.df <- rbind(meas.df, epilog)

# define totalParseMs as tdbloaderMs + jsonParseMs
epilog <- expand.grid(ExperimentName=expNames,  
                      ExperimentRun=unique(meas.df$ExperimentRun), 
                      Var=c('totalParseMs'), Value=as.double(NA));
for (i in 1:nrow(epilog)) {
  expName <- epilog$ExperimentName[i]
  run <- epilog$ExperimentRun[i]
  tmp <- meas.df[meas.df$ExperimentName==expName &  meas.df$ExperimentRun==run,];
  epilog$Value[i] <- tmp$Value[tmp$Var=='tdbloaderMs'] + tmp$Value[tmp$Var=='jsonParseMs'];
}
meas.df <- rbind(meas.df, epilog);
```

Compute statistics, mainly mean with bootstrap confidence intervals.

```{r measures}
meas.dfStat <- createStateFrame(meas.df, 1, 
                                c('selectAndPlanMs','consumeMs', 
                                  'totalParseMs', 'tdbloaderMs', 'jsonParseMs'));

plot.df <- data.frame(meas.dfStat[meas.dfStat$Var %in% c('selectAndPlanMs','consumeMs', 'totalParseMs'),])
plot.df$Var <- gsub('selectAndPlanMs', 'Select Sources & Plan', plot.df$Var)
plot.df$Var <- gsub('totalParseMs', 'Parse JSON', plot.df$Var)
plot.df$Var <- gsub('consumeMs', 'Fetch', plot.df$Var)
plot.df$Var <- factor(plot.df$Var, levels = c('Select Sources & Plan', 'Parse JSON', 'Fetch'))

ggplot(plot.df, aes(x=ExperimentName, fill=Var, y=avg)) +
  scale_y_log10(labels = scales::comma) +
  geom_bar(position = position_dodge(), stat='identity') +
  geom_errorbar(aes(ymin=avg.lo, ymax=avg.hi), position=position_dodge()) +
  theme(legend.position = 'bottom') +
  labs(x='Scenario', y='Time in milliseconds', fill='Step')

plot.df <- spread(meas.df, Var, Value)
plot.df$totalSecs <- (plot.df$selectAndPlanMs + plot.df$consumeMs + plot.df$totalParseMs)/1000.0
ggplot(plot.df, aes(x=ExperimentName, y=totalSecs)) +
  geom_bar(stat='identity') +
  scale_y_log10(labels = scales::comma) +
  #geom_errorbar(aes(ymin=avg.lo/1000, ymax=avg.hi/1000)) +
  theme(legend.position = 'bottom') +
  labs(x='Scenario', y='Time in seconds', fill='Step')
```

Build a data frame equivalent to the paper table
```{r paper.table}
paper.df <- expand.grid(Measure=c('planMs', 'executionSecs', 'parseSecs', 'loadSecs'), 
                        Scenario=c('Scenario 1', 'Scenario 2'),
                        Variant=c('Mediator', 'TDB'));
paper.df$str <- NA
paper.df$avg <- NA
paper.df$avg.int <- NA
paper.df$avg.lo <- NA
paper.df$avg.hi <- NA
for (i in 1:nrow(paper.df)) {
  isTDB <- paper.df$Variant[i]=='TDB'
  scn <- paper.df$Scenario[i]
  expName = paste(ifelse(isTDB, 'TDB', ''),
                  ifelse(scn=='Scenario 1', 'ProcurementForContract', 
                         ifelse(scn=='Scenario 2', 
                                'ProcurementsOfContractor', 
                                NA)),
                  sep='');
  name <- paper.df$Measure[i];
  varName <- ifelse(name=='planMs', 'selectAndPlanMs', 
                    ifelse(name=='executionSecs', 'consumeMs', 
                           ifelse(name=='parseSecs', 
                                  ifelse(isTDB, 'jsonParseMs', 'totalParseMs'), 
                                  ifelse(name == 'loadSecs', 
                                         ifelse(isTDB, 'tdbloaderMs', 0), 
                                         NA))))
  div <- ifelse(grepl('Secs$', name) & grepl('Ms$', varName), 1000, 1);
  tmp <- meas.dfStat[meas.dfStat$ExperimentName==expName & meas.dfStat$Var==varName,]
  paper.df$avg[i] <- tmp$avg[1]/div
  paper.df$avg.lo[i] <- tmp$avg.lo[1]/div
  paper.df$avg.hi[i] <- tmp$avg.hi[1]/div
}
paper.df$avg.int <- paper.df$avg.hi - paper.df$avg.lo
paper.df$str <- paste(round(paper.df$avg, 2), '\\pm', round(paper.df$avg.int, 2), sep='');
paper.df
```

Compute total statistics about the crawl

```{r r005}
crawl.df <- rbind(
  read.csv('../crawl/crawler-times.csv'),
  read.csv('../crawl/participante-licitacao/crawler-times.csv'),
  read.csv('../crawl/fornecedor-autocomplete/crawler-times.csv'),
  read.csv('../crawl/contratos-id/crawler-times.csv'),
  read.csv('../crawl/contratos/crawler-times.csv'),
  read.csv('../crawl/licitacoes/crawler-times.csv')
)
msecs <- sum(crawl.df$msecs)
hours <- floor(msecs/1000/60/60)
msecs <- msecs - hours*60*60*1000
minutes <- floor(msecs/1000/60)
msecs <- msecs - minutes*60*1000
seconds <- floor(msecs/1000)
paste(hours,'hours',minutes,'minutes',seconds,'seconds','for',nrow(crawl.df),'requests averaging ',mean(crawl.df$msecs),'ms/req')
```

