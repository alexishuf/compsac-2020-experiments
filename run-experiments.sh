#!/bin/bash
CRAWL_URL=${CRAWL_URL:-https://ndownloader.figshare.com/files/21805053?private_link=bc97c8b6622522961a13}
CRAWL=${CRAWL:-crawl.tar.gz}
EXPERIMENTS=${EXPERIMENTS:-experiments}
TEMP_DATA=${TEMP_DATA:-nobck}
DROP_READ_CACHE_CMD="${DROP_READ_CACHE_CMD:-sudo su -c 'echo 3 > /proc/sys/vm/drop_caches'}"
SKIP_MEDIATOR=${SKIP_MEDIATOR:-no}
SKIP_TDB=${SKIP_TDB:-no}
SKIP_LOAD=${SKIP_LOAD:-no}

get_abs_filename() {
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

rotate() {
  if [ -e "$1" ]; then
    SUFF="$(date -Iminutes)"
    echo "$1 exists, moving to \"$1.$SUFF\""
    if ( ! mv "$1" "$1.$SUFF" ); then
      echo "Failed to rotate $1. Will ignore"
    fi
  fi
}

usage() {
  echo -e "Usage: $0 [-c CRAWL.tar.gz] [-e EXPERIMENTS_DIR] [-t TEMP_DATA]\n" \
          "\n" \
          " -c CRAWL.tar.gz (default: crawl.tar.gz) \n" \
          "   A tar.gz file with all crawled data. This file will be \n
              extracted multiple times and will not be modified.\n" \
          " -e EXPERIMENTS_DIR (default: experiments)\n" \
          "   Root dir were to store experiment results. Every experiment \n" \
          "   will have its own subdirectory.\n" \
          " -t TEMP_DATA (default: nobck))\n" \
          "   Directory where the following data will be generated/extracted:\n" \
          "   crawled JSONs extracted from given CRAWL.tar.gz, NT file \n" \
          "   converted from the crawled data, and TDB2 store.\n" \
          " -M\n" \
          "   Skip the mediator experiments.\n" \
          " -T\n" \
          "   Skip the TDB2 experiments.\n" \
          " -L\n" \
          "   Skip the JSON parse and TDB2 load experiments" \
          "Environment Variables:\n" \
          " DROP_READ_CACHE_CMD=\"$DROP_READ_CACHE_CMD\n" \
          "   This command will be performed before on experiments tagged\n" \
          "   with '-flushed' "
}

while getopts "c:e:MTL" o; do
  case $o in
  c)
    CRAWL="${OPTARG}"
    ;;
  e)
    EXPERIMENTS="${OPTARG}"
    ;;
  t)
    TEMP_DATA="${OPTARG}"
    ;;
  M)
    SKIP_MEDIATOR=yes
    ;;
  T)
    SKIP_TDB=yes
    ;;
  L)
    SKIP_LOAD=yes
    ;;
  h)
    usage
    exit 0
    ;;
  *)
    usage
    exit 1
    ;;
  esac
done

check_dir() {
  mkdir -p "$1"
  if [ ! -d "$1" ]; then
    echo "Directory $1 does not exist and could not be created"
    exit 1
  fi
}
check_dir "$EXPERIMENTS" || exit 1
check_dir "$TEMP_DATA" || exit 1

mkdir -p "$TEMP_DATA"
if [ ! -d "$TEMP_DATA" ]; then
  echo "Directory $TEMP_DATA does not exist and could not be created!"
  exit 1
fi

if [ ! -e "$CRAWL" ]; then
  echo "Input crawl archive $CRAWL missing. Will try download from $CRAWL_URL"
  check_dir "$(dirname "$CRAWL")" || exit 1
  wget -O "$CRAWL" "$CRAWL_URL"
fi

CRAWL="$(get_abs_filename "$CRAWL")"
EXPERIMENTS="$(get_abs_filename "$EXPERIMENTS")"
TEMP_DATA="$(get_abs_filename "$TEMP_DATA")"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

LOG="$EXPERIMENTS/run-experiments.log"
rotate "$LOG"

echo "Extracting and parsing JSONs from $CRAWL into $TEMP_DATA."
echo "Experiments results will be in $EXPERIMENTS."
echo "This log is being written to $LOG."

drop_caches() {
  if [ -z "$DROP_READ_CACHE_CMD" ]; then
    echo  "Will not drop read caches, since DROP_READ_CACHE_CMD=\"\""
  else
    echo "Dropping read caches using DROP_READ_CACHE_CMD=$DROP_READ_CACHE_CMD"
    OLD_MB=$(free -m | sed -En 's/Mem:.* ([0-9]+)[^0-9]+[0-9]+$/\1/p')
    for i in $(seq 5); do
      if ( ! $DROP_READ_CACHE_CMD ); then
        echo "Failed to drop read caches. Exit code: $?"
        return 1
      fi
      sleep 1s
    done
    REM_MB=$(free -m | sed -En 's/Mem:.* ([0-9]+)[^0-9]+[0-9]+$/\1/p')
    echo "Freed up $(($OLD_MB - $REM_MB)) MiB"
  fi
}


setup_tdb() {
  SETUP_TDB_FLUSH=${SETUP_TDB_FLUSH:-no}
  SETUP_TDB_EXTRA_EXTRACT=${SETUP_TDB_EXTRA_EXTRACT:-}

  rm -fr "$TEMP_DATA/tmp"
  mkdir -p "$TEMP_DATA/tmp"
  cd "$TEMP_DATA/tmp" || exit 1
  echo "Extracting $CRAWL into $TEMP_DATA/tmp ..."
  if ( ! tar zxf "$CRAWL" ); then
    echo "Failed to extract $CRAWL"
    exit 1
  fi

  if [ "$(ls | wc -l)" != "1" ]; then
    echo "Expected a single directory within $CRAWL, got $(ls | wc -l)"
    exit 1
  fi
  EX_CRAWL="$TEMP_DATA/tmp/$(ls)"
  cd "$DIR"

  echo "sync..." ; sync
  test "$SETUP_TDB_FLUSH" != "yes" || ( drop_caches ; sync )

  if [ ! -z "$SETUP_TDB_EXTRA_EXTRACT" ]; then
    cd "$TEMP_DATA"
    rm -fr "$SETUP_TDB_EXTRA_EXTRACT" ; mkdir -p "$SETUP_TDB_EXTRA_EXTRACT"
    cd "$SETUP_TDB_EXTRA_EXTRACT"
    echo "Doing disturbance extract of $CRAWL into $TEMP_DATA/$SETUP_TDB_EXTRA_EXTRACT..."
    if ( ! tar zxf "$CRAWL" ); then
      echo "Failed to extract $CRAWL"
      exit 1
    fi
    cd "$DIR"
  fi

  test "$SETUP_TDB_FLUSH" != "yes" || ( drop_caches ; sync )

  ./run.sh RDF_CRAWL --crawl-dir "$EX_CRAWL" --out-nt "$TEMP_DATA/crawl.nt" \
    --tdb-dir "$TEMP_DATA/tdb" "$@" || exit 1
  rm -fr "$TEMP_DATA/tmp" "$TEMP_DATA/$SETUP_TDB_EXTRA_EXTRACT" "$TEMP_DATA/crawl.nt"
}

run_mediator_exp() {
  CMD_NAME="$1"
  DIR_NAME="$2"
  mkdir -p $EXPERIMENTS/$DIR_NAME
  date -Iminutes > $EXPERIMENTS/$DIR_NAME/when
  ./run.sh $CMD_NAME \
    --measurements-csv $EXPERIMENTS/$DIR_NAME/measurements.csv \
    --results-csv-basename $EXPERIMENTS/$DIR_NAME/results \
    --requests-csv-basename $EXPERIMENTS/$DIR_NAME/requests \
    --repetitions 5 --repetitions-sleep-secs 300 \
    2>&1 | tee $EXPERIMENTS/$DIR_NAME/run.log
}

run_tdb_exp() {
  CMD_NAME="$1"
  DIR_NAME="$2"
  mkdir -p $EXPERIMENTS/$DIR_NAME
  date -Iminutes > $EXPERIMENTS/$DIR_NAME/when
  ./run.sh $CMD_NAME --tdb-dir "$TEMP_DATA/tdb" \
    --measurements-csv $EXPERIMENTS/$DIR_NAME/measurements.csv \
    --results-csv-basename $EXPERIMENTS/$DIR_NAME/results \
    --repetitions 5 --repetitions-sleep-secs 5 \
    2>&1 | tee $EXPERIMENTS/$DIR_NAME/run.log
}

run_tdb_load_exp() {
  SUFFIX="$1"
  test "$SUFFIX" = "flushed" && SETUP_TDB_FLUSH=yes
  test -z "$SUFFIX" || SUFFIX="-$SUFFIX"
  CSV_FILE="$EXPERIMENTS/tdb-setup$SUFFIX.csv"
  rotate "$CSV_FILE"
  for i in $(seq 5); do
    echo "sync..." ; sync
    setup_tdb --csv-measurements "$CSV_FILE"
    rm -fr "$TEMP_DATA/tdb"
    echo "sync..." ; sync
    echo "sleep 5s" ; sleep 5s;
  done
}


work() {
  cd "$DIR"

  if [ "$SKIP_MEDIATOR" != yes ]; then
    echo -e "\n\nRunning mediator experiments" \
              "\n============================\n"
    run_mediator_exp EXP_PROC4CONTRACT      proc4contr
    run_mediator_exp EXP_PROC_OF_CONTRACTOR proc_of_contractor
  fi

  if [ "$SKIP_TDB" != yes ]; then
    echo -e "\n\nExtracting & parsing crawled data for TDB2 experiments" \
              "\n======================================================\n"

    setup_tdb || exit 1

    echo -e "\n\nRunning TDB2 experiments" \
              "\n========================\n"
    # Run TDB2 experiments
    run_tdb_exp EXP_TDB_PROC4CONTRACT      tdb_proc4contr
    run_tdb_exp EXP_TDB_PROC_OF_CONTRACTOR tdb_proc_of_contractor
  fi

  if [ "$SKIP_LOAD" != yes ]; then
    echo -e "\n\nBenchmarking crawled data parsing & load, HEATED read cache" \
              "\n===========================================================\n"
    run_tdb_load_exp | tee "$EXPERIMENTS/tdb-setup.log"

    echo -e "\n\nBenchmarking crawled data parsing & load, COLD read cache" \
              "\n===========================================================\n"
    run_tdb_load_exp flushed | tee "$EXPERIMENTS/tdb-setup-flushed.log"
  fi
}

work 2>&1 | tee "$LOG"
