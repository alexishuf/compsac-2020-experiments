#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
JAR_PATH=compsac-experiments/target/compsac-experiments-1.0-SNAPSHOT.jar
JAVA=${JAVA:-java}
JAVA_NINE_OPTS=${JAVA_NINE_OPTS:---add-opens java.base/java.lang=ALL-UNNAMED}
JAVA_MEM=${JAVA_MEM:--Xmx1G -Xms1G}
MVN=${MVN:-./mvnw}

echo "./run.sh arguments {"
for i in $*; do
  echo "    $i"
done
echo "}"

cd "$DIR"
if [ ! -e "$JAR_PATH" ]; then
  "$MVN" package -DskipTests=true
else
  LOGFILE=$(mktemp)
  echo -n "Rebuilding jar (will skip tests)..."
  if ! ("$MVN" package -DskipTests=true &> "$LOGFILE"); then
    echo ""
    cat "$LOGFILE" 1>&2
  else
    echo -e " OK\n"
  fi
  rm -f "$LOGFILE"
fi

time "$JAVA" $JAVA_NINE_OPTS $JAVA_MEM -jar "$JAR_PATH" "$@"
