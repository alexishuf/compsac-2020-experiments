COMPSAC 2020 experiments
========================

This repository contains code for running and analyzing the results presented 
in the COMPSAC 2020 submission titled *Query-time Integration of Web APIs 
and Semantic Graph Databases* authored by Alexis Huf, Fábio Reina and 
Frank Siqueira. 


Quick reference
---------------

1. Crawl APIs into crawl.tar.gz: `./crawl-all.sh -z crawl.tar.gz crawl`
2. Run all experiments: `./run-experiments.sh`
3. Analyze experiments: `R/analysis.Rmd`


Crawl the Web API
-----------------

Crawling really isn't a good idea in general. But we do this to build a 
materialized triple store for comparison with the federated query mediator.

To run all crawling use the crawl-all.sh script. It takes a single required 
argument, which is the crawl base directory where files will be written to.
It will log all its output to a file crawl-all.log inside the crawl directory.
A old crawl-all.log file will be moved to crawl-all.log.old before starting 
the new file.  

```bash
./crawl-all.sh --delay-ms 1000 crawl
```

This script will take **many** hours. Neither this script nor the 
detailed commands below do the following:

- Resume a previous crawl (files will be overwritten)
- Monitor network conectivity
- Retry failed requests 

For detailed comments on each crawl sub-task, see the [CRAWL.md file](CRAWL.md).


Experiments
-----------

To simply run all experiments, run the `run-experiments.sh` script from this 
directory:

```bash
./run-experiments.sh
```

For a detailed step-by-step version see the [EXPERIMENTS.md](EXPERIMENTS.md) 
file.


Web API Molecules  
-----------------
Get examples from the swagger UIs. For convenience, the json files are under 
`compsac-experiments/src/resources`. 

- [Camara de Deputados](https://dadosabertos.camara.leg.br/swagger/api.html)
  Referred in code as "Representatives" APIs. 
  This is the brazilian equivalent ot the US House of representatives. Two 
  API endpoints are of interest:
    - `/deputados`: Lists all representatives, allowing some filters.
    - `/deputados/{id}`: Gives complete information about a representative 
      given its representative ID, including its CPF (tax personal 
      identification number)
- [Portal da Transparência](http://www.transparencia.gov.br/swagger-ui.html) 
  Referred in code as "Budget" APIs. 
  This set of APIs covers mainly execution of the federal government budget. 
  Of particular interest to experiments are the endpoints: 
    - `/orgaos-siafi`: Allows listing or searching for a organization 
      within the federal government
    - `/licitacoes`: Allows listing procurements within a organization and 
      a specific 30-day period
    - `/licitacoes/{id}`: Gets all information of a specific procurement
    - `/contratos`: Lists contracts by their time of activity, given a 
      contracting government organization
    - `/contratos/{id}`: Gets information for a contract by its global ID

The examples are converted into plain `WebAPICQEndpoint` instances by 
`Example2Molecule`. This helper class has a builder-like API and some of 
the APIs endpoints listed above require some additional strategy objects 
to be associated, such as `PagingStrategy` and `TermSerializer` to control 
how parameters in URI templates are filled (e.g., date format conversions).

The actual definition of the endpoints is in factory methods from classes 
`BudgetEndpoints` and `RepresentativesEndpoints`. `Example2Molecules` ideally 
should be included as part of riefederator in a more generalized version. 
