package br.ufsc.lapesd.riefederator.data;

import br.ufsc.lapesd.riefederator.BaseApp;
import br.ufsc.lapesd.riefederator.experiments.Measurements;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.impl.MappedJsonResponseParser;
import com.google.common.base.Stopwatch;
import com.google.gson.JsonElement;
import org.apache.commons.io.FileUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.tdb2.TDB2Factory;
import org.apache.jena.tdb2.loader.DataLoader;
import org.apache.jena.tdb2.loader.LoaderFactory;
import org.apache.jena.tdb2.loader.base.LoaderOps;
import org.apache.jena.tdb2.loader.base.MonitorOutput;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.jena.rdf.model.ResourceFactory.createTypedLiteral;

public class RDFizeCrawl extends BaseApp {
    private static Logger logger = LoggerFactory.getLogger(RDFizeCrawl.class);
    private static final Pattern DATE_BR_RX = Pattern.compile("^ *(\\d\\d)/(\\d\\d)/(\\d\\d\\d\\d) *$");
    private static final Pattern CNPJ_RX = Pattern.compile("(\\d\\d.\\d\\d\\d.\\d\\d\\d/\\d\\d\\d\\d-\\d\\d)");

    @Option(name = "--crawl-dir", required = true,
            usage = "Root directory containing crawled data")
    private File crawlDir = null;

    @Option(name = "--out-nt", required = true,
            usage = "Where to save the resulting NT file. Will overwrite")
    private File outNT = null;

    @Option(name = "--delete-nt",
            usage = "Remove the NT file after TDB load, if --tdb-dir is given.")
    private boolean deleteNT = false;

    @Option(name = "--tdb-dir",
            usage = "Where to create a TDB2 directory with the data from the NT")
    private File tdbDir = null;

    @Option(name = "--ns-uri-prefix", usage = "URI prefix used to map properties")
    private String nsPrefix = "https://portaldatransparencia.example.org/ns/";

    @Option(name = "--data-uri-prefix", usage = "URI prefix used to create URIs for the " +
            "the crawl files (avoiding relative URIs)")
    private String dataUriPrefix = "https://portaldatransparencia.example.org/data/";

    @Option(name = "--csv-measurements", usage = "Target CSV file with measurements data. " +
            "If omitted, no data will be saved. If the target file exists, it will be appended to")
    private @Nullable File csvMeasurements;
    private @Nullable Measurements measurements;


    public static void main(String[] args) throws Exception {
        new RDFizeCrawl().run(args);
    }

    @Override
    protected void run() throws Exception {
        checkArgument(crawlDir.exists(), "--crawl-dir="+crawlDir+" does not exist");
        checkArgument(crawlDir.isDirectory(), "--crawl-dir="+crawlDir+" is not a directory");
        createParentDir(outNT);
        if (csvMeasurements != null) {
            measurements = new Measurements(csvMeasurements, "RDF_CRAWL");
        }

        if (tdbDir != null) {
            checkArgument(!tdbDir.getAbsoluteFile().equals(new File(".").getAbsoluteFile()),
                          "Using working directory as --tdb-dir is not safe!");
            File homeDir = new File(System.getProperty("user.home")).getAbsoluteFile();
            checkArgument(!tdbDir.getAbsoluteFile().equals(homeDir),
                          "Using $HOME ("+homeDir+") directory as --tdb-dir is not safe!");
            createParentDir(tdbDir);
            checkArgument(!tdbDir.exists() || tdbDir.isDirectory(),
                          "--tdb-dir="+tdbDir+" exists, but is not a directory");
            if (tdbDir.exists()) {
                String[] files = tdbDir.list();
                assert files != null;
                if (files.length > 0)
                    logger.warn("Non-empty --tdb-dir="+tdbDir+". Will RECURSIVELY delete files");
            }
        }

        Stopwatch sw = Stopwatch.createStarted();
        parseFilesIntoNT();
        logSeconds("Parsed jsons into", "jsonParseSecs", sw);

        sw.reset().start();
        addCnpjAsSecondName();
        logSeconds("Added CNPJ as second name in", "addCnpjSecs", sw);

        if (tdbDir != null) {
            loadToTdb(outNT);
            if (deleteNT && !outNT.delete())
                logger.error("Tried to delete {}, but failed", outNT);
        }
        if (measurements != null)
            measurements.write();
    }

    private void logSeconds(@Nonnull String messageBegin, @Nonnull String varName,
                            @Nonnull Stopwatch sw) {
        double secs = sw.elapsed(MILLISECONDS) / 1000.0;
        logger.info(messageBegin+" {} in {} seconds", outNT, secs);
        if (measurements != null)
            measurements.save(varName, secs);
    }

    private void loadToTdb(@Nonnull File outNT) throws IOException {
        checkArgument(!tdbDir.exists() || tdbDir.isDirectory());
        createParentDir(tdbDir);
        if (tdbDir.exists())
            FileUtils.deleteDirectory(tdbDir);

        Stopwatch sw = Stopwatch.createStarted();
        Dataset ds = TDB2Factory.connectDataset(tdbDir.getAbsolutePath());
        MonitorOutput out = LoaderOps.outputTo(System.out);
        DataLoader loader = LoaderFactory.phasedLoader(ds.asDatasetGraph(), out);
        loader.startBulk();
        loader.load(outNT.getAbsolutePath());
        loader.finishBulk();
        logSeconds("Created TDB database from", "tdbloaderSecs", sw);
    }

    private void addCnpjAsSecondName() throws IOException {
        try (FileOutputStream out = new FileOutputStream(outNT, true)) {
            File directory = new File(crawlDir, "fornecedor-autocomplete");
            Model model2 = ModelFactory.createDefaultModel();
            for (File file : FileUtils.listFiles(directory, new String[]{"json"}, false)) {
                Model model = parse(file);
                Property prop = ResourceFactory.createProperty(nsPrefix + "name");
                List<Statement> statements;
                statements = model.listStatements(null, prop, (RDFNode) null).toList();
                for (Statement stmt : statements) {
                    String old = stmt.getObject().asLiteral().getLexicalForm();
                    Matcher m = CNPJ_RX.matcher(old);
                    if (m.find())
                        model2.add(stmt.getSubject(), prop, createTypedLiteral(m.group(1)));
                }
            }
            if (model2.isEmpty()) {
                logger.warn("No :name cnpj triples added for fornecedor-autocomplete");
            } else {
                logger.info("Adding {} :name cnpj triples for fornecedor-autocomplete",
                            model2.size());
                RDFDataMgr.write(out, model2, RDFFormat.NT);
            }
        }
    }

    private void parseFilesIntoNT() throws IOException {
        Stopwatch sw = Stopwatch.createStarted();
        int files = 0, triples = 0;
        try (FileOutputStream out = new FileOutputStream(outNT)) {
            writeModalidadesNT(out);
            for (File file : FileUtils.listFiles(crawlDir, new String[]{"json"}, true)) {
                Model model;
                try {
                    model = parse(file);
                    ++files;
                    triples += model.size();
                } catch (IOException e) {
                    logger.error("Exception while reading from {}. Skipping file", file, e);
                    continue;
                }
                try {
                    RDFDataMgr.write(out, model, RDFFormat.NT);
                } catch (RuntimeException e) {
                    logger.error("Exception writing to output NT {}", outNT);
                    throw e;
                } finally {
                    model.close();
                }
                if (sw.elapsed(TimeUnit.SECONDS) > 10) {
                    sw.reset().start();
                    logger.info("Parsed {} files and {} triples", files, triples);
                }
            }

        }
    }

    private void writeModalidadesNT(OutputStream out) {
        String path = "br/ufsc/lapesd/riefederator/sources/modalidades.ttl";
        try (InputStream in = ClassLoader.getSystemResourceAsStream(path)) {
            assert in != null;
            Model model = ModelFactory.createDefaultModel();
            RDFDataMgr.read(model, in, Lang.TTL);
            RDFDataMgr.write(out, model, RDFFormat.NT);
        } catch (IOException ex) {
            throw new RuntimeException("IOException from resource file", ex);
        }
    }

    private @Nonnull Model parse(@Nonnull File file) throws IOException {
        Map<String, String> map = new HashMap<>();
        MappedJsonResponseParser parser = new MappedJsonResponseParser(map, nsPrefix) {
            @Override
            protected RDFNode parseInto(@Nonnull Model model, @Nonnull JsonElement element,
                                        @Nullable String subjectHint) {
                if (element.isJsonPrimitive()) {
                    Matcher m = DATE_BR_RX.matcher(element.getAsString());
                    if (m.matches()) {
                        String lex = format("%s-%s-%s", m.group(3), m.group(2), m.group(1));
                        return createTypedLiteral(lex, XSDDatatype.XSDdate);
                    }
                }
                return super.parseInto(model, element, subjectHint);
            }
        };
        String string = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        File absFile = file.getAbsoluteFile();
        File absCrawlDir = this.crawlDir.getAbsoluteFile();
        StringBuilder uriBuilder = new StringBuilder();
        uriBuilder.append(absFile.getName());
        for (File p = absFile.getParentFile(); !p.equals(absCrawlDir); p = p.getParentFile())
            uriBuilder.insert(0, p.getName() + "/");
        String uri = uriBuilder.insert(0, dataUriPrefix).toString();

        return parser.parseAsModel(string, null, uri);
    }

    private void createParentDir(File file) throws IOException {
        File parent = file.getParentFile();
        checkArgument(!parent.exists() || parent.isDirectory(),
                      parent+" exists, but is not a directory");
        if (!parent.exists() && !parent.mkdirs())
            throw new IOException("Failed to mkdir "+parent);
    }
}
