package br.ufsc.lapesd.riefederator.experiments;

import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import br.ufsc.lapesd.riefederator.webapis.requests.HTTPRequestInfo;
import br.ufsc.lapesd.riefederator.webapis.requests.HTTPRequestObserver;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Arrays.asList;

public class Requests2CSV implements HTTPRequestObserver {
    private static final Logger logger = LoggerFactory.getLogger(Requests2CSV.class);
    private static final List<String> HEADERS = asList(
            "method", "uri", "dateTimeISO", "status", "responseBytes", "contentType",
            "createUriMs", "setupMs", "requestMs", "parseMs",
            "jsonRootArrayMembers", "jsonRootObjectMembers", "parsedTriples",
            "exception"
    );

    private @Nonnull File destination;
    private boolean append;
    private boolean wrote = false;
    private List<HTTPRequestInfo> infos = new ArrayList<>(4096);

    public Requests2CSV(@Nonnull File destination, boolean append) {
        checkArgument(!destination.exists() || destination.isFile(),
                      "Destination "+destination+" exists, but is a directory!");
        this.destination = destination;
        this.append = append;
    }

    public void install(@Nonnull Federation federation) {
        for (Source source : federation.getSources()) {
            if (source.getEndpoint() instanceof WebAPICQEndpoint)
                ((WebAPICQEndpoint)source.getEndpoint()).setObserver(this);
        }
    }

    public int getRequestsCount() {
        return infos.size();
    }

    public double getTotalRequestMs() {
        return infos.stream().map(i -> Math.max(i.getRequestMs(), 0)).reduce(0.0, Double::sum);
    }

    public double getTotalParseMs() {
        return infos.stream().map(i -> Math.max(i.getParseMs(), 0)).reduce(0.0, Double::sum);
    }

    public int getTotalParsedTriples() {
        return infos.stream().map(i -> Math.max(i.getParsedTriples(), 0)).reduce(0, Integer::sum);
    }

    @Override
    public void accept(@Nonnull HTTPRequestInfo info) {
        if (wrote)
            logger.warn("write() was already called, but received info {}", info);
        else
            infos.add(info);
    }

    public void write() throws IOException {
        File parent = destination.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IOException("Could not mkdir "+parent+", required to receive " +
                                  "CSV file "+destination+" with requests log");
        }
        if (infos.size() > 2048)
            logger.info("Saving {} records to {} (append={})", infos.size(), destination, append);
        boolean writeHeaders = !destination.exists() || destination.length() == 0 || !append;
        try (FileWriter writer = new FileWriter(destination, append);
             CSVPrinter p = new CSVPrinter(writer, CSVFormat.EXCEL)) {
            if (writeHeaders)
                p.printRecord(HEADERS);
            for (HTTPRequestInfo i : infos) {
                p.printRecord(asList(
                        i.getMethod(),      i.getUri(),           i.getRequestDateAsISO(),
                        i.getStatus(),      i.getResponseBytes(), i.getContentType(),
                        i.getCreateUriMs(), i.getSetupMs(),       i.getRequestMs(),
                        i.getParseMs(),     i.getJsonRootArrayMembers(),
                        i.getJsonRootObjectMembers(),
                        i.getParsedTriples(),
                        i.getException() != null ? i.getException().getMessage() : ""
                ));
            }
        }
        logger.info("Saved {} records ({} bytes) to {} (append={})",
                    infos.size(), destination.length(), destination, append);
        wrote = true;
    }
}
