package br.ufsc.lapesd.riefederator.data;

import br.ufsc.lapesd.riefederator.BaseApp;
import com.google.common.base.Preconditions;
import com.google.gson.GsonBuilder;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileAlreadyExistsException;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;

public class Table2Document extends BaseApp {
    private static final Logger logger = LoggerFactory.getLogger(Table2Document.class);

    @Option(name = "--zips-dir", usage = "Directory with zip files to read", required = true)
    private File zipsDir;

    @Option(name = "--create-skeleton-map", usage = "Create a new skeleton of the " +
            "column -> JSON property mappings")
    private File createSkeleton;

    public static void main( String[] args ) throws Exception {
        new Table2Document().run(args);
    }

    @Override
    protected void run() throws Exception {
        if (createSkeleton != null)
            doCreateSkeleton();
    }

    private void doCreateSkeleton() throws IOException {
        Preconditions.checkState(createSkeleton != null);
        if (createSkeleton.exists())
            throw new  FileAlreadyExistsException(createSkeleton.getAbsolutePath());
        Preconditions.checkArgument(zipsDir != null, "--zips-dir not given");
        Map<String, Map<String, String>> table2col2prop = new HashMap<>();
        ZipsDir dir = new ZipsDir(this.zipsDir);
        for (TablesZip zip : dir.getZips()) {
            for (ZipEntry entry : zip.getFiles()) {
                TableFile table = zip.open(entry);
                Map<String, String> col2prop;
                col2prop = table2col2prop.computeIfAbsent(table.getName(), n -> new HashMap<>());
                for (String column : table.getColumns()) {
                    String prop = convertToProp(column);
                    String old = col2prop.get(column);
                    if (old != null && !old.equals(prop)) {
                        logger.error("Conflict for column {} from table {}. Old property" +
                                     "was {}, current is {}", column, table, old, prop);
                    } else {
                        col2prop.put(column, prop);
                    }
                }
                logger.info("Processed {}", table);
            }
        }
        try (FileWriter writer = new FileWriter(createSkeleton)) {
            new GsonBuilder().setPrettyPrinting().create().toJson(table2col2prop, writer);
        }
    }

    private @Nonnull String convertToProp(@Nonnull String column) {
        String normalized = Normalizer.normalize(column, Normalizer.Form.NFD);
        column = normalized.replaceAll("[^\\p{ASCII}]", "");

        StringBuilder b = new StringBuilder();
        String[] pieces = column.split(" +");
        b.append(pieces[0].toLowerCase());
        for (int i = 1; i < pieces.length; i++) {
            b.append(pieces[i].substring(0, 1).toUpperCase());
            String rest = pieces[i].substring(1);
            if (rest.toUpperCase().equals(rest))
                b.append(rest); //all upper: it is an acronym
            else
                b.append(rest.toLowerCase());
        }
        return b.toString();
    }
}
