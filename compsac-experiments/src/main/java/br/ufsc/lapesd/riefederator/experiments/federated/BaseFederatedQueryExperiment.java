package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.experiments.BaseQueryExperiment;
import br.ufsc.lapesd.riefederator.experiments.Measurements;
import br.ufsc.lapesd.riefederator.experiments.Requests2CSV;
import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.model.prefix.StdPrefixDict;
import br.ufsc.lapesd.riefederator.model.term.Term;
import br.ufsc.lapesd.riefederator.model.term.Var;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.query.EstimatePolicy;
import br.ufsc.lapesd.riefederator.query.Results;
import br.ufsc.lapesd.riefederator.query.Solution;
import br.ufsc.lapesd.riefederator.query.modifiers.ModifierUtils;
import br.ufsc.lapesd.riefederator.query.modifiers.Projection;
import br.ufsc.lapesd.riefederator.sources.BudgetEndpoints;
import com.google.common.base.Stopwatch;
import org.apache.commons.csv.CSVPrinter;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static br.ufsc.lapesd.riefederator.query.Capability.PROJECTION;
import static java.util.stream.Collectors.toList;

public abstract class BaseFederatedQueryExperiment extends BaseQueryExperiment {
    private static final Logger logger = LoggerFactory.getLogger(BaseFederatedQueryExperiment.class);

    @Option(name = "--requests-csv-basename", required = true,
            usage = "Path and basename of csv with **requests** information. Will receive the " +
                    "\"-$run.csv\" suffix for each repetition")
    private String requestsCsvBasename;

    @Option(name = "--repetitions-sleep-secs", usage = "How many SECONDS to sleep between " +
            "each repetition. Consider 5 minutes or more.")
    private int repetitionSleepSecs = 5*60;

    protected Federation federation = null;

    protected @Nonnull Collection<Source> getSources() {
        return BudgetEndpoints.all();
    }

    protected @Nonnull Federation createFederation(@Nonnull Collection<Source> sources) {
        Federation federation = Federation.createDefault();
        sources.forEach(federation::addSource);
        federation.setEstimatePolicy(EstimatePolicy.local(100));
        return federation;
    }

    protected @Nonnull String term2csvCell(@Nonnull String varName, @Nullable Term term) {
        if (term == null) return "";
        else if (term.isLiteral()) return term.asLiteral().getLexicalForm();
        else return term.toString(StdPrefixDict.EMPTY);
    }

    protected abstract  @Nonnull CQuery createQuery();

    @Override
    protected void run() {
        checkOptions();
        mkdirsParent(requestsCsvBasename);

        if (measurementsFile.exists())
            logger.info("{} exists. Will be appended to.", measurementsFile);
        if (repetitions > 1) {
            logger.info("Will do {} repetitions with interval of {} seconds",
                    repetitions, repetitionSleepSecs);
        }

        for (int i = 0; i < repetitions; i++) {
            if (i > 0) {
                try {
                    Thread.sleep(repetitionSleepSecs*1000);
                } catch (InterruptedException e) {
                    logger.error("Interrupted while sleeping between runs!");
                    throw new RuntimeException(e);
                }
            }
            try {
                run(i);
            } catch (Exception e) {
                logger.error("Exception on {} at {}-th run out of {}", name(), i, repetitions, e);
            }
        }
        try {
            federation.close();
        } finally {
            federation = null;
        }
    }

    protected void run(int run) throws Exception {
        measurement = new Measurements(measurementsFile, name(), run);
        ArrayDeque<Solution> list = new ArrayDeque<>(2048);

        Stopwatch sw = Stopwatch.createStarted();
        if (federation == null) {
            Collection<Source> sources = getSources();
            measurement.saveAndRestart("getSourcesMs", sw);
            federation = createFederation(sources);
            measurement.saveAndRestart("createFederationMs", sw);
        }
        File requestsCsvFile = new File(requestsCsvBasename + "-" + run + ".csv");
        Requests2CSV requests2CSV = new Requests2CSV(requestsCsvFile, false);
        requests2CSV.install(federation);

        CQuery query = createQuery();
        measurement.saveAndRestart("createQueryMs", sw);

        Stopwatch sw2 = Stopwatch.createStarted();
        Results results = federation.query(query);
        measurement.saveAndRestart("selectAndPlanMs", sw2);
        results.forEachRemainingThenClose(list::add);
        measurement.saveAndRestart("consumeMs", sw2);
        measurement.saveAndRestart("selectPlanAndConsumeMs", sw);
        long bytes = writeSolutions(query, list, run);
        measurement.saveAndRestart("writeResultsMs", sw);

        measurement.save("resultsCount", list.size());
        measurement.save("resultsCsvBytes", bytes);
        measurement.save("requestsCount", requests2CSV.getRequestsCount());
        measurement.save("totalRequestMsWithoutSleep", requests2CSV.getTotalRequestMs());
        measurement.save("totalParseMs", requests2CSV.getTotalParseMs());
        measurement.save("totalParsedTriples", requests2CSV.getTotalParsedTriples());
        measurement.write();
        requests2CSV.write();
    }

    protected long writeSolutions(@Nonnull CQuery query, @Nonnull Collection<Solution> list,
                                  int run) throws IOException {
        List<String> columns = query.streamTerms(Var.class).map(Var::getName)
                                    .distinct().collect(toList());
        Projection project = (Projection) ModifierUtils.getFirst(PROJECTION, query.getModifiers() );
        if (project != null) {
            columns.retainAll(project.getVarNames());
        }

        File file = new File(resultsCsvBasename + "-" + run + ".csv");
        try (FileWriter writer = new FileWriter(file);
             CSVPrinter printer = new CSVPrinter(writer, CSV_FMT)) {
            printer.printRecord(columns);
            ArrayList<String> row = new ArrayList<>(columns.size());
            for (Solution solution : list) {
                for (String name : columns)
                    row.add(term2csvCell(name, solution.get(name)));
                printer.printRecord(row);
                row.clear();
            }
        }
        return new File(file.getAbsolutePath()).length();
    }
}
