package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.webapis.description.PureDescriptive;

import javax.annotation.Nonnull;

public class ProcurementForContract extends BaseFederatedQueryExperiment {
    @Override
    protected @Nonnull CQuery createQuery() {
        Lit name = lit("Universidade Federal de Santa Catarina");
        return CQuery.with(
                             /*  Get contracts given date and organization code */
                /*  0 */ new Triple(x,  p("dataInicioVigencia"), date("2019-01-01")),
                /*  1 */ new Triple(x,  p("dataFimVigencia"),   date("2019-12-31")),
                /*  2 */ new Triple(x,  p("dataAssinatura"),    r3),
                /*  3 */ new Triple(x,  p("unidadeGestora"), x1),
                /*  4 */ new Triple(x1, p("orgaoVinculado"), x2),
                /*  5 */ new Triple(x2, p("codigoSIAFI"),    t),
                /*  6 */ new Triple(x,  p("id"),          b),
                /*  7 */ new Triple(x , p("dimCompra"), x3),
                /*  8 */ new Triple(x3, p("numero"),    n),
                /*  9 */ new Triple(x,  p("unidadeGestora"), x4),
                /* 10 */ new Triple(x4, p("codigo"),         u),
                             /* Get organization code from its name */
                /* 11 */ new Triple(z,  p("descricao"), name),
                /* 12 */ new Triple(z,  p("codigo"),    t),
                             /* Get modality of procurement from the contract by id */
                /* 13 */ new Triple(w,  p("id"),               b),
                /* 14 */ new Triple(w,  p("modalidadeCompra"), w1),
                /* 15 */ new Triple(w1, p("descricao"),        d),
                             /* Get modality code from its description */
                /* 16 */ new Triple(m,  Modalities.hasDescription, d),
                /* 17 */ new Triple(m,  Modalities.hasCode,        c),
                             /* Get the originating procurement  */
                /* 18 */ new Triple(y,  p("unidadeGestora"), y1),
                /* 19 */ new Triple(y1, p("codigo"),         u),
                /* 20 */ new Triple(y,  p("licitacao"), y2),
                /* 21 */ new Triple(y2, p("numero"),    n),
                /* 22 */ new Triple(y,  p("modalidadeLicitacao"), y3),
                /* 23 */ new Triple(y3, p("codigo"),              c),
                /* 24 */ new Triple(y,  p("id"),                  r1),
                /* 25 */ new Triple(y,  p("dataResultadoCompra"), r2))
                .annotate( 0, PureDescriptive.INSTANCE)
                .annotate( 1, PureDescriptive.INSTANCE)
                .annotate(23, PureDescriptive.INSTANCE)
                .build();
    }

    public static void main(String[] args) throws Exception {
        new ProcurementForContract().run(args);
    }
}
