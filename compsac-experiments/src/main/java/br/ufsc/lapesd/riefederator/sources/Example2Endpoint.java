package br.ufsc.lapesd.riefederator.sources;

import br.ufsc.lapesd.riefederator.description.Molecule;
import br.ufsc.lapesd.riefederator.description.molecules.Atom;
import br.ufsc.lapesd.riefederator.description.molecules.MoleculeBuilder;
import br.ufsc.lapesd.riefederator.model.term.std.StdPlain;
import br.ufsc.lapesd.riefederator.model.term.std.StdURI;
import br.ufsc.lapesd.riefederator.query.Cardinality;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import br.ufsc.lapesd.riefederator.webapis.description.APIMolecule;
import br.ufsc.lapesd.riefederator.webapis.requests.impl.UriTemplateExecutor;
import br.ufsc.lapesd.riefederator.webapis.requests.paging.PagingStrategy;
import br.ufsc.lapesd.riefederator.webapis.requests.paging.impl.NoPagingStrategy;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.TermSerializer;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.impl.MappedJsonResponseParser;
import br.ufsc.lapesd.riefederator.webapis.requests.rate.RateLimitsRegistry;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.uri.UriTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.*;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

public class Example2Endpoint {
    private static final Logger logger = LoggerFactory.getLogger(Example2Endpoint.class);
    private static final Pattern FILENAME_RX = Pattern.compile("^.*/?([^/]+)$");
    private static final Pattern EXT_RX = Pattern.compile("^(.*)\\.([^.]+)$");
    private static final AtomicInteger nextAnonId = new AtomicInteger();

    private @Nonnull JsonElement root;
    private @Nonnull String name;
    private @Nonnull String uriPrefix = StdPlain.URI_PREFIX;
    private @Nullable UriTemplate uriTemplate = null;
    private @Nonnull Map<String, String> atom2Input = new HashMap<>();
    private @Nonnull Map<String, TermSerializer> input2serializer = new HashMap<>();
    private @Nonnull PagingStrategy pagingStrategy = NoPagingStrategy.INSTANCE;
    private boolean optionalByDefault = false;
    private @Nonnull Set<String> optional = new HashSet<>();
    private @Nonnull Set<String> required = new HashSet<>();
    private @Nonnull RateLimitsRegistry rateLimitsRegistry = new RateLimitsRegistry();
    private @Nullable Cardinality cardinality = null;

    public Example2Endpoint(@Nonnull JsonElement element, @Nonnull String name) {
        this.root = element;
        this.name = name;
    }

    public @Nonnull Example2Endpoint uriPrefix(@Nonnull String uriPrefix) {
        this.uriPrefix = uriPrefix;
        return this;
    }

    public @Nonnull Example2Endpoint name(@Nonnull String name) {
        this.name = name;
        return this;
    }

    public @Nonnull Example2Endpoint rateLimitsRegistry(@Nonnull RateLimitsRegistry registry) {
        this.rateLimitsRegistry = registry;
        return this;
    }

    public @Nonnull Example2Endpoint atom2Input(@Nonnull String atom, @Nonnull String input) {
        if (uriTemplate != null) {
            checkArgument(uriTemplate.getTemplateVariables().contains(input),
                          input+" is not a variable in "+uriTemplate);
        }
        atom2Input.put(atom, input);
        return this;
    }

    public @Nonnull Example2Endpoint requiredByDefault() {
        optionalByDefault = false;
        return this;
    }

    public @Nonnull Example2Endpoint optionalByDefault() {
        optionalByDefault = true;
        return this;
    }

    public @Nonnull Example2Endpoint required(@Nonnull String... inputs) {
        List<String> list = Arrays.asList(inputs);
        Set<String> dup = intersect(list, optional);
        checkArgument(dup.isEmpty(), "Some args to required() are already optional: "+dup);
        required.addAll(list);
        return this;
    }

    public @Nonnull Example2Endpoint optional(@Nonnull String... inputs) {
        List<String> list = Arrays.asList(inputs);
        Set<String> dup = intersect(list, required);
        checkArgument(dup.isEmpty(), "Some args to optional() are already required: "+dup);
        optional.addAll(list);
        return this;
    }

    public @Nonnull Example2Endpoint input2Serializer(@Nonnull String input,
                                      @Nonnull TermSerializer serializer) {
        input2serializer.put(input, serializer);
        if (uriTemplate != null) {
            checkArgument(uriTemplate.getTemplateVariables().contains(input),
                          input+" is not a variable of template "+uriTemplate);
        }
        return this;
    }

    public @Nonnull Example2Endpoint cardinality(@Nullable Cardinality cardinality) {
        this.cardinality = cardinality;
        return this;
    }

    public @Nonnull Example2Endpoint pagingStrategy(@Nonnull PagingStrategy pagingStrategy) {
        this.pagingStrategy = pagingStrategy;
        return this;
    }

    public @Nonnull Example2Endpoint uriTemplate(@Nonnull String uriTemplate) {
        return uriTemplate(new UriTemplate(uriTemplate));
    }

    public @Nonnull Example2Endpoint uriTemplate(@Nonnull UriTemplate uriTemplate) {
        HashSet<String> vars = new HashSet<>(uriTemplate.getTemplateVariables());
        for (Map.Entry<String, String> e : atom2Input.entrySet()) {
            checkArgument(vars.contains(e.getValue()), "Previously mapped to atom "+e.getKey()+
                          ", "+e.getValue()+" is not a variable in "+uriTemplate);
        }
        for (Map.Entry<String, TermSerializer> e : input2serializer.entrySet()) {
            checkArgument(vars.contains(e.getKey()), "Previously mapped to serializer"+
                          e.getValue()+", "+e.getKey()+" is not a variable in "+uriTemplate);
        }
        this.uriTemplate = uriTemplate;
        return this;
    }

    public static @Nonnull Example2Endpoint fromResource(@Nonnull String path) {
        InputStream in;
        in = Example2Endpoint.class.getResourceAsStream(path);
        if (in == null && !path.startsWith("/"))
            in = Example2Endpoint.class.getResourceAsStream("/"+path);
        if (in == null) {
            ClassLoader cl = ClassLoader.getSystemClassLoader();
            in = cl.getResourceAsStream(path);
            if (in == null && path.startsWith("/"))
                in = cl.getResourceAsStream(path.substring(1));
        }
        checkArgument(in != null, "Resource not found: "+path);

        Matcher filenameMatcher = FILENAME_RX.matcher(path);
        checkArgument(filenameMatcher.matches(), path+" does not look like a filename");
        Matcher extMatcher = EXT_RX.matcher(filenameMatcher.group(1));
        String name = extMatcher.matches() ? extMatcher.group(1) : filenameMatcher.group(1);

        try (InputStreamReader reader = new InputStreamReader(in)) {
            return new Example2Endpoint(new JsonParser().parse(reader), name);
        } catch (IOException e) {
            throw new RuntimeException("IOException while reading resource file", e);
        }
    }

    public static @Nonnull Example2Endpoint fromJsonString(@Nonnull String json) {
        return fromJsonString(json, "Anon-"+nextAnonId.incrementAndGet());
    }

    public static @Nonnull Example2Endpoint fromJsonString(@Nonnull String json,
                                                           @Nonnull String name) {
        return new Example2Endpoint(new JsonParser().parse(json), name);
    }
    public static @Nonnull Example2Endpoint fromFile(@Nonnull String path) throws IOException {
        return fromFile(new File(path));
    }
    public static @Nonnull Example2Endpoint fromFile(@Nonnull File file) throws IOException {
        Matcher matcher = EXT_RX.matcher(file.getName());
        String name = matcher.matches() ? matcher.group(1) : file.getName();
        try (FileReader reader = new FileReader(file)) {
            return new Example2Endpoint(new JsonParser().parse(reader), name);
        }
    }

    public @Nonnull Molecule toMolecule() {
        checkArgument(root.isJsonArray() || root.isJsonObject(),
                "Root element of the example must be either an array or object");
        MoleculeBuilder builder = new MoleculeBuilder(name);
        if (root.isJsonArray()) {
            int objCount = 0, nonObjCount = 0;
            for (JsonElement e : root.getAsJsonArray()) {
                if (e.isJsonObject()) ++objCount;
                else ++nonObjCount;
            }
            checkArgument(objCount > 0, "root is a array, but contains no objects!");
            if (nonObjCount > 0) {
                logger.warn("Root for molecule {} is an array. Ignoring {} non-object members",
                            name, nonObjCount);
            }
            for (JsonElement e : root.getAsJsonArray()) {
                if (e.isJsonObject()) feed(builder, e.getAsJsonObject(), emptyList());
            }
        } else {
            feed(builder, root.getAsJsonObject(), emptyList());
        }
        Molecule mol = builder.closed().exclusive().build();
        int atomCount = mol.getAtomCount();
        if (atomCount <= 1)
            logger.warn("Empty molecule {} with only {} atom", name, atomCount);
        return mol;
    }

    public @Nonnull APIMolecule toAPIMolecule() {
        checkState(uriTemplate != null, "URI template not set!");
        Set<String> missing = setMinus(uriTemplate.getTemplateVariables(),
                                       union(atom2Input.values(),
                                             pagingStrategy.getParametersUsed()));
        checkState(missing.isEmpty(),
                   "Some variables in the URI template are not mapped to atoms: "+missing);
        MappedJsonResponseParser parser = new MappedJsonResponseParser(emptyMap(), uriPrefix);

        Set<String> acReq = new HashSet<>(), acOpt = new HashSet<>();
        (optionalByDefault ? acOpt : acReq).addAll(uriTemplate.getTemplateVariables());
        acReq.removeAll(optional);
        acOpt.removeAll(required);
        acOpt.addAll(optional);
        acReq.addAll(required);
        Cardinality cardinality = this.cardinality == null ? guessCardinality() : this.cardinality;

        UriTemplateExecutor executor = UriTemplateExecutor.from(uriTemplate)
                .withRequired(acReq)
                .withOptional(acOpt)
                .withSerializers(input2serializer)
                .withPagingStrategy(pagingStrategy)
                .withResponseParser(parser)
                .withRateLimitsRegistry(rateLimitsRegistry)
                .build();
        return new APIMolecule(toMolecule(), executor, atom2Input, cardinality);
    }

    private @Nonnull Cardinality guessCardinality() {
        if (root.isJsonArray())
            return Cardinality.lowerBound(2);
        else if (root.isJsonObject()) {
            Pattern linksPropertyRx = Pattern.compile("_?(links|href)");
            JsonObject o = root.getAsJsonObject();
            if (o.size() == 2) {
                int hasLink = 0, hasArrayValue = 0;
                for (Map.Entry<String, JsonElement> e : o.entrySet()) {
                    if (linksPropertyRx.matcher(e.getKey()).matches())
                        ++hasLink;
                    else if (e.getValue().isJsonArray())
                        ++hasArrayValue;
                }
                if (hasLink == 1 && hasArrayValue == 1)
                    return Cardinality.lowerBound(2);
            } else if (o.entrySet().stream().filter(e -> e.getValue().isJsonArray()).count() == 1){
                for (String name : Arrays.asList("data", "values", "elements")) {
                    if (o.has(name) && o.get(name).isJsonArray())
                        return Cardinality.lowerBound(2);
                }
            }
            return Cardinality.upperBound(1);
        }
        return Cardinality.UNSUPPORTED;
    }

    public @Nonnull WebAPICQEndpoint toEndpoint() {
        return new WebAPICQEndpoint(toAPIMolecule());
    }

    private void feed(@Nonnull MoleculeBuilder builder, @Nonnull JsonObject object,
                      @Nonnull List<String> path) {
        for (Map.Entry<String, JsonElement> e : object.entrySet()) {
            StdURI edge = uri(e.getKey());

            if (e.getValue().isJsonArray()) {
                for (JsonElement member : e.getValue().getAsJsonArray())
                    builder.out(edge, entryToAtom(path, e.getKey(), member));
            } else if (e.getValue().isJsonObject()) {
                builder.out(edge, entryToAtom(path, e.getKey(), e.getValue()));
            } else {
                builder.out(edge, new Atom(property2Atom(e.getKey(), path)));
            }
        }
    }

    private @Nonnull Atom entryToAtom(@Nonnull List<String> path,
                                      @Nonnull String property, @Nonnull JsonElement value) {
        if (value.isJsonPrimitive())
            return new Atom(property2Atom(property, path));
        Preconditions.checkArgument(value.isJsonObject());

        ArrayList<String> nextPath = new ArrayList<>(path);
        nextPath.add(property);
        MoleculeBuilder childBuilder = Molecule.builder(property2Atom(property, path));
        feed(childBuilder, value.getAsJsonObject(), nextPath);
        return childBuilder.closed().exclusive().buildAtom();
    }

    private @Nonnull StdURI uri(@Nonnull String property) {
        return new StdURI(uriPrefix + property);
    }

    private @Nonnull String property2Atom(@Nonnull String property, @Nonnull List<String> path) {
        StringBuilder b = new StringBuilder();
        property2Atom(property, b);
        for (ListIterator<String> it = path.listIterator(path.size()); it.hasPrevious(); )
            property2Atom(it.previous(), b);
        return b.toString();
    }

    private void property2Atom(@Nonnull String property, @Nonnull StringBuilder b) {
        b.append(property.substring(0, 1).toUpperCase()).append(property.substring(1));
    }
}
