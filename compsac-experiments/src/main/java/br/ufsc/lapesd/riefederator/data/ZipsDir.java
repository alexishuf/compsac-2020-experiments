package br.ufsc.lapesd.riefederator.data;

import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class ZipsDir {
    private @Nonnull File dir;

    private static Pattern ZIP_RX = Pattern.compile("(?i)\\.zip$");

    public ZipsDir(@Nonnull File dir) {
        this.dir = dir;
    }

    public @Nonnull File getDir() {
        return dir;
    }

    public List<TablesZip> getZips() throws IOException {
        File[] array = dir.listFiles((dir, name) -> ZIP_RX.matcher(name).find());
        if (array == null)
            throw new IOException("Unreadable directory " + getDir());
        List<TablesZip> list = new ArrayList<>();
        for (File file : array)
            list.add(new TablesZip(file));
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ZipsDir)) return false;
        ZipsDir zipsDir = (ZipsDir) o;
        return getDir().equals(zipsDir.getDir());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDir());
    }

    @Override
    public String toString() {
        return dir.getAbsolutePath();
    }
}
