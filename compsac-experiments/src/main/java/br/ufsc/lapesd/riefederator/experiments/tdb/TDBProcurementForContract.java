package br.ufsc.lapesd.riefederator.experiments.tdb;

import org.apache.jena.vocabulary.XSD;

import javax.annotation.Nonnull;

public class TDBProcurementForContract extends BaseTDBQueryExperiment {

    @Override
    public @Nonnull String getQuery() {
        return  "PREFIX : <"+nsUriPrefix+">\n" +
                "PREFIX mod: <"+Modalities.PREFIX+">\n" +
                "PREFIX xsd: <"+ XSD.NS+">\n" +
                "\n" +
                "SELECT * WHERE {\n" +
                "  ?x  :dataInicioVigencia ?xDateStart\n" +
                "    FILTER(?xDateStart >= \"2019-01-01\"^^xsd:date).\n" +
                "  ?x  :dataFimVigencia    ?xDateEnd\n" +
                "    FILTER(?xDateEnd <= \"2019-12-31\"^^xsd:date).\n" +
                "  ?x  :dataAssinatura     ?r3 .\n" +
                "  ?x  :unidadeGestora     ?x1 .\n" +
                "  ?x1 :orgaoVinculado     ?x2 .\n" +
                "  ?x2 :codigoSIAFI        ?t  .\n" +
                "  ?x  :id                 ?b  .\n" +
                "  ?x  :dimCompra          ?x3 .\n" +
                "  ?x3 :numero             ?n  .\n" +
                "  ?x  :unidadeGestora     ?x4 .\n" +
                "  ?x4 :codigo             ?u  .\n" +

                "  ?z  :descricao \"Universidade Federal de Santa Catarina\"^^xsd:string .\n" +
                "  ?z  :codigo    ?t .\n" +

//                "  ?w  :id               ?b .\n" +
                "  ?x  :modalidadeCompra ?w1 .\n" +
                "  ?w1 :descricao        ?d .\n" +

//                "  ?m  mod:hasDescription ?d .\n" +
//                "  ?m  mod:hasCode        ?c .\n" +

                "  ?y  :unidadeGestora      ?y1 .\n" +
                "  ?y1 :codigo              ?u .\n" +
                "  ?y  :licitacao           ?y2 .\n" +
                "  ?y2 :numero              ?n .\n" +
//                "  ?y  :modalidadeLicitacao ?y3 .\n" +
//                "  ?y3 :descricao           ?d .\n" + //match fails as terminology changes
                "  ?y  :id                  ?r1 .\n" +
                "  ?y  :dataResultadoCompra ?r2 .\n" +
                "}";
    }

    public static void main(String[] args) throws Exception {
        new TDBProcurementForContract().run(args);
    }
}
