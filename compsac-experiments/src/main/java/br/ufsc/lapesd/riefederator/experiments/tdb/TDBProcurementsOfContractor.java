package br.ufsc.lapesd.riefederator.experiments.tdb;

import org.apache.jena.vocabulary.XSD;

import javax.annotation.Nonnull;

public class TDBProcurementsOfContractor extends BaseTDBQueryExperiment {

    @Override
    public @Nonnull String getQuery() {
        return  "PREFIX : <"+nsUriPrefix+">\n" +
                "PREFIX mod: <"+Modalities.PREFIX+">\n" +
                "PREFIX xsd: <"+ XSD.NS+">\n" +
                "\n" +
                "SELECT * WHERE {\n" +
                "  ?x  :dataInicioVigencia  ?startDate \n" +
                "      FILTER(?startDate >= \"2019-01-01\"^^xsd:date) .\n" +
                "  ?x  :dataFimVigencia     ?endDate \n" +
                "      FILTER(?endDate   <= \"2019-12-31\"^^xsd:date) .\n" +
                "  ?x  :unidadeGestora      ?x1 .\n" +
                "  ?x1 :orgaoVinculado      ?x2 .\n" +
                "  ?x2 :codigoSIAFI         ?t  .\n" +
                "  ?x  :id                  ?b  .\n" +
                "  ?x  :fornecedor          ?x3 .\n" +
                "  ?x3 :codigoFormatado     ?c  .\n" +

                "  ?y  :codigo              ?t  .\n" +
                "  ?y  :descricao           \"Universidade Federal de Santa Catarina\"^^xsd:string  .\n" +

                "  ?z  :id                  ?b  .\n" +
                "  ?z  :modalidadeCompra    ?z1 .\n" +
                "  ?z1 :descricao           \"Inexigibilidade de Licitação\"^^xsd:string  .\n" +

                "  ?w  :id                  ?s  .\n" +
                "  ?w  :name                ?c  .\n" +

                "  ?u  :skLicitacao         ?a  .\n" +
                "  ?u  :idFornecedor        ?s  .\n" +

                "  ?v  :id                  ?a  .\n" +
                "  ?v  :licitacao           ?v1 .\n" +
                "  ?v  :valor               ?r1 .\n" +
                "  ?v  :modalidadeLicitacao ?v2 .\n" +
                "  ?v1 :objeto              ?r2 .\n" +
                "  ?v1 :contatoResponsavel  ?r3 .\n" +
                "  ?v2 :descricao           ?d  .\n" +

                "  ?m  mod:hasDescription   ?d  .\n" +
                "  ?m  a                    mod:ApenasPreco .\n" +
                "}\n";
    }

    public static void main(String[] args) throws Exception {
        new TDBProcurementsOfContractor().run(args);
    }
}
