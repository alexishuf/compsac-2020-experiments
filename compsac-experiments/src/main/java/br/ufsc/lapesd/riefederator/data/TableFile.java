package br.ufsc.lapesd.riefederator.data;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.apache.commons.csv.CSVFormat.EXCEL;

public class TableFile {
    private static Logger logger = LoggerFactory.getLogger(TableFile.class);
    private int year, month, rows;
    private String name;
    private List<String> columns;
    private Callable<InputStream> inSupplier;
    private Charset charset;
    private static CSVFormat FORMAT = EXCEL.withDelimiter(';').withFirstRecordAsHeader();

    private static final List<Charset> TEST_CHARSETS = Arrays.asList(StandardCharsets.ISO_8859_1,
                                                                     StandardCharsets.UTF_8);
    private static final Pattern ACCENTS_RX = Pattern.compile("[áéíóúàãẽõü]");
    private static final Pattern RX_FIELDS = Pattern.compile(
            "(?iU)(\\d\\d\\d\\d)(\\d\\d)_(\\w+)\\.csv(?:\\.\\w+)?$");
    //           ^             ^        ^
    //           |             |        +---> [3] name
    //           |             +---> [2] month
    //           +---> [1] year

    private void init(@Nonnull String path,
                      @Nonnull Callable<InputStream> inSupplier) throws IOException {
        Matcher matcher = RX_FIELDS.matcher(path);
        Preconditions.checkArgument(matcher.find(), "Bad filename syntax: "+path);
        year = Integer.parseInt(matcher.group(1));
        month = Integer.parseInt(matcher.group(2));
        name = matcher.group(3);
        Preconditions.checkArgument(name != null);
        this.inSupplier = inSupplier;
        charset = guessCharset(inSupplier);
        try (CSVParser records = FORMAT.parse(new InputStreamReader(inSupplier.call(), charset))) {
            columns = records.getHeaderNames();
            int count = 0;
            for (Iterator<CSVRecord> it = records.iterator(); it.hasNext(); it.next())
                ++count;
            rows = count;
        } catch (Exception e) {
            if (e instanceof IOException)      throw (IOException)e;
            if (e instanceof RuntimeException) throw (RuntimeException)e;
            else                               throw new RuntimeException(e);
        }
    }

    private @Nonnull Charset guessCharset(@Nonnull Callable<InputStream> inSupplier) {
        for (Charset charset : TEST_CHARSETS) {
            try (InputStreamReader in = new InputStreamReader(inSupplier.call(), charset);
                 CSVParser records = FORMAT.parse(in)) {
                List<String> columns = records.getHeaderNames();
                boolean nonAscii = false, ptAccents = false;
                for (String name : columns) {
                    if (!CharMatcher.ascii().matchesAllOf(name)) {
                        nonAscii = true;
                        if (ACCENTS_RX.matcher(name).find())
                            ptAccents = true;
                    }
                }
                if (!nonAscii || ptAccents)
                    return charset; //valid charset: all names are ASCII or it has valid accents
            } catch (Exception ignored) { }
        }
        logger.error("Could not guess a valid encoding! will use UTF-8, but data seems wrong");
        return StandardCharsets.UTF_8;
    }

    public static boolean isValidName(@Nonnull String path) {
        return RX_FIELDS.matcher(path).find();
    }

    public TableFile(@Nonnull String path) throws IOException {
        Preconditions.checkArgument(!TablesZip.isZipFilename(path));
        init(path, () -> new FileInputStream(path));
    }

    public TableFile(@Nonnull ZipFile file, @Nonnull ZipEntry entry,
                     @Nonnull String cleanName) throws IOException {
        init(cleanName, () -> file.getInputStream(entry));
    }

    public @Nonnull CSVParser parse() throws IOException {
        try {
            return FORMAT.parse(new InputStreamReader(inSupplier.call(), charset));
        } catch (Exception e) {
            if (e instanceof IOException)           throw (IOException)e;
            else if (e instanceof RuntimeException) throw (RuntimeException)e;
            else                                    throw new RuntimeException(e);
        }
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getRows() {
        return rows;
    }

    public @Nonnull String getName() {
        return name;
    }

    public List<String> getColumns() {
        return columns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TableFile)) return false;
        TableFile tableFile = (TableFile) o;
        return getYear() == tableFile.getYear() &&
                getMonth() == tableFile.getMonth() &&
                Objects.equals(getName(), tableFile.getName()) &&
                Objects.equals(getColumns(), tableFile.getColumns());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getYear(), getMonth(), getName(), getColumns());
    }

    public @Nonnull String toString() {
        return String.format("%04d%02d_%s.csv{rows=%d}",
                getYear(), getMonth(), getName(), getRows());
    }
}
