package br.ufsc.lapesd.riefederator.data;

import com.google.common.base.CharMatcher;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class TablesZip {
    private static Pattern ZIP_RX = Pattern.compile("(?i)\\.zip$");

    private @Nonnull ZipFile zipFile;
    private boolean iso88591Hack;
    private static final Pattern ACCENTS_RX = Pattern.compile("[áéíóúàãẽõü]");

    public static boolean isZipFilename(@Nonnull String path) {
        return ZIP_RX.matcher(path).find();
    }

    public TablesZip(@Nonnull ZipFile file) {
        zipFile = file;
    }

    public TablesZip(@Nonnull File file) throws IOException {
        // zipped CSVs from http://www.portaldatransparencia.gov.br/download-de-dados
        // come with ISO-8859-1 filename encodings, which causes a nasty exception from
        // nexElement() in checkZipFileEncoding() and other places when the zip file
        // is read in UTF-8.
        ZipFile iso88591 = new ZipFile(file, StandardCharsets.ISO_8859_1);
        if (!checkZipFileEncoding(iso88591)) {
            ZipFile utf8 = new ZipFile(file, StandardCharsets.UTF_8);
            if (checkZipFileEncoding(utf8)) { //bad UTF-8 (prone to exception throwing)
                zipFile = utf8;
                iso88591Hack = false;
            } else {
                zipFile = iso88591;
                iso88591Hack = true;
            }
        } else {
            zipFile = iso88591;
            iso88591Hack = false;
        }
    }

    private static boolean checkZipFileEncoding(ZipFile file) {
        boolean nonAscii = false;
        boolean ptAccents = false;
        try {
            for (Enumeration<? extends ZipEntry> e = file.entries(); e.hasMoreElements(); ) {
                String name = e.nextElement().getName();
                if (!CharMatcher.ascii().matchesAllOf(name)) {
                    nonAscii = true;
                    if (ACCENTS_RX.matcher(name).find())
                        ptAccents = true;
                }
            }
        } catch (RuntimeException e) {
            return false; //consider exceptions as a bad encoding choice
        }
        return !nonAscii || ptAccents;
    }

    public @Nonnull ZipFile getZipFile() {
        return zipFile;
    }

    public boolean getIso88591Hack() {
        return iso88591Hack;
    }

    public @Nonnull List<ZipEntry> getFiles() {
        List<ZipEntry> list = new ArrayList<>();
        for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = e.nextElement();
            String name = cleanName(entry);
            if (TableFile.isValidName(name))
                list.add(entry);
        }
        return list;
    }

    private String cleanName(@Nonnull ZipEntry entry) {
        if (!getIso88591Hack()) return entry.getName();
        String name = entry.getName();
        name = name.replaceAll("\u0087", "ç");
        name = name.replaceAll("Æ", "ã");
        return name;
    }

    public @Nonnull TableFile open(@Nonnull ZipEntry e) throws IOException {
        return new TableFile(getZipFile(), e, cleanName(e));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TablesZip)) return false;
        TablesZip tablesZip = (TablesZip) o;
        return getZipFile().equals(tablesZip.getZipFile());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getZipFile());
    }

    @Override
    public String toString() {
        return zipFile.getName();
    }
}
