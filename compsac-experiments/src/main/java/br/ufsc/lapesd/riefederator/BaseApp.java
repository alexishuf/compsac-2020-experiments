package br.ufsc.lapesd.riefederator;

import br.ufsc.lapesd.riefederator.experiments.VocabContext;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import javax.annotation.Nonnull;
import java.io.PrintStream;

public abstract class BaseApp extends VocabContext {
    @Option(name = "--help", aliases = {"-h"}, help = true, usage = "Shows help")
    protected boolean help = false;

    public void run(String[] args) throws Exception {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);
            if (this.help)
                printHelp(parser, System.out);
            else
                this.run();
        } catch (CmdLineException e) {
            System.err.println(e.getLocalizedMessage());
            printHelp(parser, System.err);
        }
    }

    protected static void printHelp(@Nonnull CmdLineParser parser, @Nonnull PrintStream out) {
        out.print("Usage:\n  ");
        parser.printSingleLineUsage(out);
        out.println("Options: ");
        parser.printUsage(out);
    }

    protected abstract void run() throws Exception;
}
