package br.ufsc.lapesd.riefederator.experiments.tdb;

import br.ufsc.lapesd.riefederator.experiments.BaseQueryExperiment;
import br.ufsc.lapesd.riefederator.experiments.Measurements;
import com.google.common.base.Stopwatch;
import org.apache.commons.csv.CSVPrinter;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.system.Txn;
import org.apache.jena.tdb2.TDB2Factory;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.TimeUnit.MICROSECONDS;

public abstract class BaseTDBQueryExperiment extends BaseQueryExperiment {
    private static final Logger logger = LoggerFactory.getLogger(BaseTDBQueryExperiment.class);

    @Option(name = "--tdb-dir", usage = "Directory with triples extracted from crawled JSONs",
            required = true)
    private File tdbDir;

    @Option(name = "--repetitions-sleep-secs", usage = "How many SECONDS to sleep between " +
            "each repetition.")
    private int repetitionSleepSecs = 5;

    @Option(name = "--ns-uri-prefix", usage = "URI prefix for properties")
    protected String nsUriPrefix = "https://portaldatransparencia.example.org/ns/";

    protected @Nonnull String node2csvCell(@Nonnull String varName, @Nullable RDFNode term) {
        if (term == null) return "";
        else if (term.isLiteral()) return term.asLiteral().getLexicalForm();
        else return term.toString();
    }

    public abstract @Nonnull String getQuery();

    protected @Nonnull Dataset getDataset() {
        return TDB2Factory.connectDataset(tdbDir.getAbsolutePath());
    }

    @Override
    protected void run() {
        checkOptions();
        if (repetitions > 1) {
            logger.info("Will do {} repetitions with interval of {} seconds",
                    repetitions, repetitionSleepSecs);
        }

        for (int i = 0; i < repetitions; i++) {
            if (i > 0) {
                try {
                    Thread.sleep(repetitionSleepSecs*1000);
                } catch (InterruptedException e) {
                    logger.error("Interrupted while sleeping between repetitions");
                    throw new RuntimeException(e);
                }
            }
            try {
                run(i);
            } catch (Exception e) {
                logger.error("Exception on {} at {}-th run out of {}", name(), i, repetitions, e);
            }
        }
    }

    private void run(int run)  {
        measurement = new Measurements(measurementsFile, name(), run);
        List<QuerySolution> list = new ArrayList<>();

        Stopwatch sw = Stopwatch.createStarted();
        Dataset dataset = getDataset();
        measurement.saveAndRestart("openTDBMs", sw);

        CompletableFuture<List<String>> resultVarsFuture = new CompletableFuture<>();
        String query = getQuery();
        Stopwatch sw3 = Stopwatch.createStarted();
        Txn.executeRead(dataset, () -> {
            try (QueryExecution exec = QueryExecutionFactory.create(query, dataset)) {
                ResultSet resultSet = exec.execSelect();
                measurement.save("selectAndPlanMs", sw.elapsed(MICROSECONDS)/1000.0);
                Stopwatch sw2 = Stopwatch.createStarted();
                resultVarsFuture.complete(resultSet.getResultVars());
                resultSet.forEachRemaining(list::add);
                measurement.saveAndRestart("consumeMs", sw2);
                measurement.saveAndRestart("selectPlanAndConsumeMs", sw);
            }

            logger.info("Got {} results in {}", list.size(), sw3.elapsed(MICROSECONDS)/1000.0);

            List<String> resultVars = resultVarsFuture.getNow(null);

            long bytes = 0;
            try {
                bytes = writeSolutions(resultVars, list, run);
            } catch (IOException ex) {
                logger.error("IOException saving results", ex);
            }

            measurement.saveAndRestart("writeResultsMs", sw);
            measurement.save("resultsCount", list.size());
            measurement.save("resultsCsvBytes", bytes);
            try {
                measurement.write();
            } catch (IOException ex) {
                logger.error("IOException saving measurement to", ex);
            }
        });

    }

    private long writeSolutions(@Nonnull List<String> columns, @Nonnull List<QuerySolution> list,
                                int run) throws IOException {
        File file = new File(resultsCsvBasename + "-" + run + ".csv");
        try (FileWriter writer = new FileWriter(file);
             CSVPrinter printer = new CSVPrinter(writer, CSV_FMT)) {
            printer.printRecord(columns);
            ArrayList<String> row = new ArrayList<>(columns.size());
            for (QuerySolution solution : list) {
                for (String name : columns)
                    row.add(node2csvCell(name, solution.get(name)));
                printer.printRecord(row);
                row.clear();
            }
        }
        return new File(file.getAbsolutePath()).length();
    }
}
