package br.ufsc.lapesd.riefederator.sources;

import br.ufsc.lapesd.riefederator.experiments.VocabContext;
import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.model.term.Term;
import br.ufsc.lapesd.riefederator.model.term.std.StdLit;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.query.Solution;
import br.ufsc.lapesd.riefederator.query.impl.MapSolution;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import br.ufsc.lapesd.riefederator.webapis.description.PureDescriptive;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static br.ufsc.lapesd.riefederator.jena.JenaWrappers.fromJena;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.jena.datatypes.xsd.XSDDatatype.XSDdate;
import static org.apache.jena.datatypes.xsd.XSDDatatype.XSDstring;
import static org.apache.jena.rdf.model.ResourceFactory.createTypedLiteral;
import static org.testng.Assert.*;

/**
 * Does test invocations against real WebAPIs
 */
public class BudgetEndpointsTest extends VocabContext {
    @Test
    public void testGetDescriptionOfOrganization() {
        StdLit code = StdLit.fromUnescaped("20101");

        WebAPICQEndpoint ep = BudgetEndpoints.organizationsByCode();
        CQuery query = CQuery.with(new Triple(x, p("codigo"), code),
                                   new Triple(x, p("descricao"), y)).project(y).build();
        List<Term> list = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(s -> list.add(s.get(y)));
        assertEquals(list, singletonList(StdLit.fromUnescaped("Presidência da República")));
    }

    @Test
    public void testOrganizationByDescription() {
        WebAPICQEndpoint ep = BudgetEndpoints.organizationsByDescription();
        CQuery query = CQuery.from(new Triple(x, p("descricao"), lit("Presidência da República")),
                                   new Triple(x, p("codigo"), y));
        List<Term> l = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(s -> l.add(s.get(y)));
        assertEquals(l, singletonList(lit("20101")));
    }

    @Test
    public void testGetContractWithSpecificDates() {
        //data is a xsd:string, term serializer is tolerant to that.
        //Then reason is that the parser will parse it as string, so this allows matching
        Lit date = fromJena(createTypedLiteral("13/05/2019"));
        assertEquals(date.getDatatype().getURI(), XSDstring.getURI());
        Lit organization = lit("26246"); //university
        WebAPICQEndpoint ep = BudgetEndpoints.contracts();

        CQuery query = CQuery.from(new Triple(x, p("dataInicioVigencia"), date),
                                   new Triple(x, p("dataFimVigencia"), date),
                                   new Triple(x, p("id"), y),
                                   new Triple(x, p("unidadeGestora"), z),
                                   new Triple(z, p("orgaoVinculado"), w),
                                   new Triple(w, p("codigoSIAFI"), organization));

        List<Term> list = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(s -> list.add(s.get(y)));
        assertTrue(list.contains(fromJena(createTypedLiteral(58834986))));
    }

    @Test
    public void testGetContractsForUniversity() {
        Lit start = fromJena(createTypedLiteral("2019-04-01", XSDdate));
        Lit end = fromJena(createTypedLiteral("2020-01-31", XSDdate));
        Lit organization = lit("26246");
        WebAPICQEndpoint ep = BudgetEndpoints.contracts();

        CQuery query = CQuery.with(new Triple(x, p("dataInicioVigencia"), start),
                                   new Triple(x, p("dataFimVigencia"), end),
                                   new Triple(x, p("id"), y),
                                   new Triple(x, p("unidadeGestora"), z),
                                   new Triple(z, p("orgaoVinculado"), w),
                                   new Triple(w, p("codigoSIAFI"), organization))
                .annotate(0, PureDescriptive.INSTANCE)
                .annotate(1, PureDescriptive.INSTANCE)
                .build();

        List<Term> list = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(s -> list.add(s.get(y)));

        // contract from page 1: kitchen exhaust
        assertTrue(list.contains(fromJena(createTypedLiteral(49635430))));
        // contracts from page 2: walkway maintenance and chemicals acquisition
        assertTrue(list.contains(fromJena(createTypedLiteral(58834986))));
        assertTrue(list.contains(fromJena(createTypedLiteral(61536114))));
    }

    @Test
    public void testGetContractById() {
        WebAPICQEndpoint ep = BudgetEndpoints.contractById();
        List<Term> list = new ArrayList<>();
        ep.query(CQuery.from(new Triple(x, p("id"), fromJena(createTypedLiteral(49635430))),
                             new Triple(x, p("situacaoContrato"), y),
                             new Triple(y, p("descricao"), lit("Publicado")),
                             new Triple(x, p("valorInicialCompra"), z)))
                .forEachRemainingThenClose(s -> list.add(s.get(z)));
        assertEquals(list, singletonList(fromJena(createTypedLiteral(59818.17d))));
    }

    @Test
    public void testGetProcurementsForUniversityHospital() {
        WebAPICQEndpoint ep = BudgetEndpoints.procurements();
        Lit start = fromJena(createTypedLiteral("2019-01-01", XSDdate));
        Lit end = fromJena(createTypedLiteral("2019-01-31", XSDdate));
        CQuery query = CQuery.with(new Triple(x, p("dataAberturaMinima"), start),
                                   new Triple(x, p("dataAberturaMaxima"), end),
                                   new Triple(x, p("unidadeGestora"), y),
                                   new Triple(y, p("nome"), lit("HOSPITAL UNIVERSITÁRIO/UFSC")),
                                   new Triple(y, p("orgaoVinculado"), u),
                                   new Triple(u, p("codigoSIAFI"), lit("26246")),
                                   new Triple(x, p("id"), z),
                                   new Triple(x, p("valor"), w))
                .annotate(0, PureDescriptive.INSTANCE)
                .annotate(1, PureDescriptive.INSTANCE)
                .project(z, w).build();

        List<Solution> list = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(list::add);

        assertTrue(list.contains(MapSolution.builder() //from page 1 of results
                .put(z, fromJena(createTypedLiteral(259753239)))
                .put(w, fromJena(createTypedLiteral(45500))).build()));
        assertTrue(list.contains(MapSolution.builder() //from page 2 of results
                .put(z, fromJena(createTypedLiteral(265147213)))
                .put(w, fromJena(createTypedLiteral(7650))).build()));
        assertTrue(list.contains(MapSolution.builder() //from page 3 of results
                .put(z, fromJena(createTypedLiteral(272363302)))
                .put(w, fromJena(createTypedLiteral(140447.51d))).build()));
    }

    @Test
    public void testGetProcurementById() {
        WebAPICQEndpoint ep = BudgetEndpoints.procurementById();
        Lit id = fromJena(createTypedLiteral(259753239));
        CQuery query = CQuery.from(new Triple(x, p("id"), id),
                                   new Triple(x, p("licitacao"), y),
                                   new Triple(y, p("numero"), z),
                                   new Triple(x, p("valor"), v),
                                   new Triple(x, p("municipio"), w),
                                   new Triple(w, p("nomeIBGE"), u));
        List<Solution> list = new ArrayList<>();
        ep.query(query).forEachRemainingThenClose(list::add);
        assertEquals(list.size(), 1);

        assertEquals(list.get(0).get(z), lit("000062019"));
        assertEquals(list.get(0).get(u), lit("FLORIANÓPOLIS"));
        assertEquals(list.get(0).get(v), fromJena(createTypedLiteral(45500)));
    }

    @Test
    public void testFederatedProcurementsForUniversityHospital() {
        Federation federation = Federation.createDefault(); //create a federation
        BudgetEndpoints.all().forEach(federation::addSource); //add all sources

        //write a query:
        Lit start = fromJena(createTypedLiteral("2019-01-01", XSDdate));
        Lit end = fromJena(createTypedLiteral("2019-01-31", XSDdate));
        Lit uni = lit("Universidade Federal de Santa Catarina");
        CQuery query = CQuery.with(new Triple(x, p("dataAberturaMinima"), start),
                                   new Triple(x, p("dataAberturaMaxima"), end),
                                   new Triple(t, p("descricao"), uni), // orgaos-siafi
                                   new Triple(t, p("codigo"), s),      // orgaos-siafi
                                   new Triple(x, p("unidadeGestora"), y),
                                   new Triple(y, p("nome"), lit("HOSPITAL UNIVERSITÁRIO/UFSC")),
                                   new Triple(y, p("orgaoVinculado"), u),
                                   new Triple(u, p("codigoSIAFI"), s),
                                   new Triple(x, p("id"), z),
                                   new Triple(x, p("valor"), w))
                .annotate(0, PureDescriptive.INSTANCE)
                .annotate(1, PureDescriptive.INSTANCE)
                .annotate(2, PureDescriptive.INSTANCE) //not required!
                .project(z, w).build();

        List<Solution> list = new ArrayList<>();
        federation.query(query).forEachRemainingThenClose(list::add); //execute!

        assertTrue(list.contains(MapSolution.builder()
                .put(z, fromJena(createTypedLiteral(259753239)))
                .put(w, fromJena(createTypedLiteral(45500))).build()));
        assertTrue(list.contains(MapSolution.builder()
                .put(z, fromJena(createTypedLiteral(265147213)))
                .put(w, fromJena(createTypedLiteral(7650))).build()));
        assertTrue(list.contains(MapSolution.builder()
                .put(z, fromJena(createTypedLiteral(272363302)))
                .put(w, fromJena(createTypedLiteral(140447.51d))).build()));
    }

    @Test
    public void testGetContractorAutocomplete() {
        WebAPICQEndpoint ep = BudgetEndpoints.contractorByFreetext();
        List<Term> ids = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("name"), lit("04.196.645/0001-00")),
                             new Triple(x, p("id"), y))
                .annotate(0, PureDescriptive.INSTANCE).build()
        ).forEachRemainingThenClose(s -> ids.add(s.get(y)));
        assertEquals(ids, singletonList(lit("27942321")));
    }

    @Test
    public void testGetContractsByContractorId() {
        WebAPICQEndpoint ep = BudgetEndpoints.contractsByContractorId();
        List<Term> list = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("idFornecedor"), lit("19844197")),
                             new Triple(x, p("valorContratado"), v))
                .annotate(0, PureDescriptive.INSTANCE).build())
                .forEachRemainingThenClose(s -> list.add(s.get(v)));
        assertFalse(list.isEmpty());

        List<String> values = list.stream()
                .map(t -> t.asLiteral().getLexicalForm()).collect(toList());
        assertFalse(values.isEmpty());
        assertTrue(values.contains("657.000,00"));
    }

    @Test
    public void testGetPaymentsByCnpj() {
        WebAPICQEndpoint ep = BudgetEndpoints.paymentsReceivedByCnpj();
        List<String> values = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("cnpj"), lit("05664394000104")),
                             new Triple(x, p("fase"), lit("Pagamento")),
                             new Triple(x, p("valor"), v))
                .annotate(0, PureDescriptive.INSTANCE).build())
                .forEachRemainingThenClose(
                        s -> values.add(requireNonNull(s.get(v)).asLiteral().getLexicalForm()));
        assertFalse(values.isEmpty());
        assertTrue(values.contains("15.592,50"));
    }

    @Test
    public void testFederatedGetContractsByContractorName() {
        Federation federation = Federation.createDefault(); //create a federation
        BudgetEndpoints.all().forEach(federation::addSource); //add all sources

        List<String> values = new ArrayList<>();
        Lit name = lit("ATOMED PRODUTOS MEDICOS E DE AUXILIO HUMANO LTDA");
        federation.query(CQuery.with(new Triple(x, p("name"), name),
                                     new Triple(x, p("id"), u),
                                     new Triple(y, p("idFornecedor"), u),
                                     new Triple(y, p("valorContratado"), v))
                .annotate(0, PureDescriptive.INSTANCE)
                .annotate(2, PureDescriptive.INSTANCE)
                .build()
        ).forEachRemainingThenClose(
                s -> values.add(requireNonNull(s.get(v)).asLiteral().getLexicalForm()));
        assertTrue(values.contains("657.000,00"));
    }

    @Test
    public void testProcurementWithContractor() {
        WebAPICQEndpoint ep = BudgetEndpoints.procurementsWithContractor();
        List<String> ids = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("idFornecedor"), lit("101817370")),
                             new Triple(x, p("skLicitacao"), y))
                .annotate(0, PureDescriptive.INSTANCE).build()
        ).forEachRemainingThenClose(s
                -> ids.add(requireNonNull(s.get(y)).asLiteral().getLexicalForm()));
        assertFalse(ids.isEmpty());
        assertTrue(ids.contains("265145670")); //pg 2
    }

    @Test
    public void testProcurementByUnityModalityAndNumber() {
        WebAPICQEndpoint ep = BudgetEndpoints.procurementByUnitModalityAndNumber();
        List<String> list = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("unidadeGestora"),      u),
                             new Triple(u, p("codigo"),              lit("153163")),
                             new Triple(x, p("modalidadeLicitacao"), t),
                             new Triple(t, p("codigo"),              lit("6")),
                             new Triple(x, p("licitacao"),           y),
                             new Triple(y, p("numero"),              lit("006102018")),
                             new Triple(x, p("id"),                  z))
                .annotate(3, PureDescriptive.INSTANCE).build()
        ).forEachRemainingThenClose(
                s -> list.add(requireNonNull(s.get(z)).asLiteral().getLexicalForm()));
        assertEquals(list, singletonList("268370792"));
    }
}