Experiments
===========

The paper describes two scenarios (queries) executed against the federation and
against a TDB that contains RDF data converted from the crawled JSONs.

Federation Experiments
----------------------

The first scenario is a query that gets some properties of the procurement 
that originated every contract of a organization that was active in 2019: 
```bash
mkdir -p experiments/proc4contr
date > experiments/proc4contr/when
./run.sh EXP_PROC4CONTR \
    --measurements-csv experiments/proc4contr/measurements.csv \
    --results-csv-basename experiments/proc4contr/results \
    --requests-csv-basename experiments/proc4contr/requests \
    --repetitions 5 \
    --repetitions-sleep-secs 300 \
    2>&1 | tee experiments/proc4contr/run.log
```


The second scenario is a query that gets the non-directed procurements 
where a contractor of a contract spawned from a directed procurement 
participated:
```bash
RUN=1
mkdir -p experiments/proc_of_contractor-$RUN
date > experiments/proc_of_contractor-$RUN/when
./run.sh EXP_PROC_OF_CONTRACTOR \
    --measurements-csv experiments/proc_of_contractor-$RUN/measurements.csv \
    --results-csv-basename experiments/proc_of_contractor-$RUN/results \
    --requests-csv-basename experiments/proc_of_contractor-$RUN/requests \
    --repetitions 5 \
    --repetitions-sleep-secs 300 \
    2>&1 | tee experiments/proc_of_contractor-$RUN/run.log
```

TDB2 experiments
---------------- 

The previous experiments have counterparts using TDB2, an open source triple 
store. To execute those experiments, crawled jsons must be converted to RDF 
and loaded into TDB2:
```bash
mkdir -p nobck
./run.sh RDF_CRAWL --crawl-dir crawl --out-nt nobck/crawl.nt \
    --tdb-dir nobck/tdb 2>&1 | tee nobck/rdf_crawl.log
```

To run the TDB version of the first scenario:
```bash
mkdir -p experiments/tdb_proc4contr
date > experiments/tdb_proc4contr/when
./run.sh EXP_TDB_PROC4CONTRACT --tdb-dir nobck/tdb \
    --measurements-csv experiments/tdb_proc4contr/measurements.csv \
    --results-csv-basename experiments/tdb_proc4contr/results \
    --repetitions 5 --repetitions-sleep-secs 5 \
    2>&1 | tee experiments/tdb_proc4contr/run.log
```

To run the TDB version of the second scenario:
```bash
mkdir -p experiments/tdb_proc_of_contractor
date > experiments/tdb_proc_of_contractor/when
./run.sh EXP_TDB_PROC_OF_CONTRACTOR --tdb-dir nobck/tdb \
    --measurements-csv experiments/tdb_proc_of_contractor/measurements.csv \
    --results-csv-basename experiments/tdb_proc_of_contractor/results \
    --repetitions 5 --repetitions-sleep-secs 5 \
    2>&1 | tee experiments/tdb_proc_of_contractor/run.log
```

### Note about disk read caches.

The parsing of JSON crawled files and their loading into TDB can be timed 
if the `--csv-measurements` option is provided:

```bash
mkdir -p nobck
./run.sh RDF_CRAWL --crawl-dir crawl \
    --out-nt nobck/crawl.nt \
    --tdb-dir nobck/tdb \
    --csv-measurements "experiments/tdb-setup.csv"
2>&1 | tee nobck/rdf_crawl.log
```

The crawled data comprises too many files. Walking the filesystem tree strains 
the operating system with IO operations that slow down the read throughput. 
The OS will cache the directory entries, so that further walks will be faster. 
Therefore, after extracting a tar.gz with thousands of documents, reading all 
those documents will be fast due to the hot cache of directory entries. On the 
other hand, reading a large directory that was created before the machine was 
booted up will be slow.

On linux, disk read caches can be dropped with the 
`sudo su -c 'echo 3 > /proc/sys/vm/drop_caches'` command. The 
[run-experiments.sh](run-experiments.sh) script will sample parse & load times 
in two scenarios: without dropping cache, right after tar.gz extraction and 
dropping cache (`-flushed.csv` variant) right after extraction.   
 